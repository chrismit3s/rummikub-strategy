use crate::color::{Color, ColorSet};
use crate::number::{Number, NumberSet};
use crate::set::Set;
use crate::table::TableIndex;
use std::char;
use std::fmt::{self, Display};
use std::iter::{self, Iterator, Map};
use std::ops::Range;
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};


pub type PieceIter =
    Map<
        Map<
            Range<usize>,
            fn(usize) -> Result<Piece, String>>,
        fn(Result<Piece, String>) -> Piece>;


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Piece {
    pub number: Number,
    pub color: Color,
}

impl Piece {
    pub const NUM_PIECES: usize = Color::NUM_COLORS * Number::NUM_NUMBERS;

    pub fn index_for(number: Number, color: Color) -> usize {
        number.index() * Color::NUM_COLORS + color.index()
    }

    pub fn new(number: Number, color: Color) -> Piece {
        Piece { number, color }
    }

    pub fn from_index(index: usize) -> Result<Piece, String> {
        let color = Color::from_index(index % Color::NUM_COLORS).unwrap();
        let number = Number::from_index(index / Color::NUM_COLORS).unwrap();
        Ok(Piece { number, color })
    }

    pub fn value(&self) -> u8 {
        self.number.value()
    }

    pub fn minimal_sets(&self) -> impl Iterator<Item = Set> + '_ {
        let min_start = self.number.index().saturating_sub(Set::MIN_LEN - 1);
        let max_start = self.number.index().min(Number::NUM_NUMBERS - Set::MIN_LEN);
        iter::once(Set::group(self.number, ColorSet::full(), 0).unwrap())
            .chain((min_start..=max_start).map(move |start| Set::run(NumberSet::from_run(start, Set::MIN_LEN), self.color, 0).unwrap()))
    }

    pub fn iter() -> PieceIter {
        (0..Piece::NUM_PIECES)
            .map(Piece::from_index as fn(usize) -> Result<Piece, String>)
            .map(Result::unwrap)
    }
}

impl TableIndex for Piece {
    fn index(&self) -> usize {
        Piece::index_for(self.number, self.color)
    }
}

impl TableIndex for &Piece {
    fn index(&self) -> usize {
        self.number.index() * Color::NUM_COLORS + self.color.index()
    }
}

impl Display for Piece {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}", self.color, self.number)
    }
}

impl FromStr for Piece {
    type Err = String;

    fn from_str(s: &str) -> Result<Piece, String> {
        if let Some((color_string, number_string)) = s.find(char::is_numeric).map(|i| s.split_at(i)) {
            Ok(Piece::new(number_string.parse()?, color_string.parse()?))
        }
        else {
            Err(format!("{} does not end with a tile value", s))
        }
    }
}

impl Distribution<Piece> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Piece {
        Piece { number: rng.gen(), color: rng.gen() }
    }
}


pub mod values {
    use super::*;
    use crate::number::values::*;

    pub const RED01: Piece = Piece { number: N01, color: Color::Red };
    pub const RED02: Piece = Piece { number: N02, color: Color::Red };
    pub const RED03: Piece = Piece { number: N03, color: Color::Red };
    pub const RED04: Piece = Piece { number: N04, color: Color::Red };
    pub const RED05: Piece = Piece { number: N05, color: Color::Red };
    pub const RED06: Piece = Piece { number: N06, color: Color::Red };
    pub const RED07: Piece = Piece { number: N07, color: Color::Red };
    pub const RED08: Piece = Piece { number: N08, color: Color::Red };
    pub const RED09: Piece = Piece { number: N09, color: Color::Red };
    pub const RED10: Piece = Piece { number: N10, color: Color::Red };
    pub const RED11: Piece = Piece { number: N11, color: Color::Red };
    pub const RED12: Piece = Piece { number: N12, color: Color::Red };
    pub const RED13: Piece = Piece { number: N13, color: Color::Red };
    pub const BLU01: Piece = Piece { number: N01, color: Color::Blue };
    pub const BLU02: Piece = Piece { number: N02, color: Color::Blue };
    pub const BLU03: Piece = Piece { number: N03, color: Color::Blue };
    pub const BLU04: Piece = Piece { number: N04, color: Color::Blue };
    pub const BLU05: Piece = Piece { number: N05, color: Color::Blue };
    pub const BLU06: Piece = Piece { number: N06, color: Color::Blue };
    pub const BLU07: Piece = Piece { number: N07, color: Color::Blue };
    pub const BLU08: Piece = Piece { number: N08, color: Color::Blue };
    pub const BLU09: Piece = Piece { number: N09, color: Color::Blue };
    pub const BLU10: Piece = Piece { number: N10, color: Color::Blue };
    pub const BLU11: Piece = Piece { number: N11, color: Color::Blue };
    pub const BLU12: Piece = Piece { number: N12, color: Color::Blue };
    pub const BLU13: Piece = Piece { number: N13, color: Color::Blue };
    pub const BLK01: Piece = Piece { number: N01, color: Color::Black };
    pub const BLK02: Piece = Piece { number: N02, color: Color::Black };
    pub const BLK03: Piece = Piece { number: N03, color: Color::Black };
    pub const BLK04: Piece = Piece { number: N04, color: Color::Black };
    pub const BLK05: Piece = Piece { number: N05, color: Color::Black };
    pub const BLK06: Piece = Piece { number: N06, color: Color::Black };
    pub const BLK07: Piece = Piece { number: N07, color: Color::Black };
    pub const BLK08: Piece = Piece { number: N08, color: Color::Black };
    pub const BLK09: Piece = Piece { number: N09, color: Color::Black };
    pub const BLK10: Piece = Piece { number: N10, color: Color::Black };
    pub const BLK11: Piece = Piece { number: N11, color: Color::Black };
    pub const BLK12: Piece = Piece { number: N12, color: Color::Black };
    pub const BLK13: Piece = Piece { number: N13, color: Color::Black };
    pub const YLW01: Piece = Piece { number: N01, color: Color::Yellow };
    pub const YLW02: Piece = Piece { number: N02, color: Color::Yellow };
    pub const YLW03: Piece = Piece { number: N03, color: Color::Yellow };
    pub const YLW04: Piece = Piece { number: N04, color: Color::Yellow };
    pub const YLW05: Piece = Piece { number: N05, color: Color::Yellow };
    pub const YLW06: Piece = Piece { number: N06, color: Color::Yellow };
    pub const YLW07: Piece = Piece { number: N07, color: Color::Yellow };
    pub const YLW08: Piece = Piece { number: N08, color: Color::Yellow };
    pub const YLW09: Piece = Piece { number: N09, color: Color::Yellow };
    pub const YLW10: Piece = Piece { number: N10, color: Color::Yellow };
    pub const YLW11: Piece = Piece { number: N11, color: Color::Yellow };
    pub const YLW12: Piece = Piece { number: N12, color: Color::Yellow };
    pub const YLW13: Piece = Piece { number: N13, color: Color::Yellow };
    pub const ALL: &'static [Piece] = &[
                values::RED01, values::BLU01, values::BLK01, values::YLW01,
                values::RED02, values::BLU02, values::BLK02, values::YLW02,
                values::RED03, values::BLU03, values::BLK03, values::YLW03,
                values::RED04, values::BLU04, values::BLK04, values::YLW04,
                values::RED05, values::BLU05, values::BLK05, values::YLW05,
                values::RED06, values::BLU06, values::BLK06, values::YLW06,
                values::RED07, values::BLU07, values::BLK07, values::YLW07,
                values::RED08, values::BLU08, values::BLK08, values::YLW08,
                values::RED09, values::BLU09, values::BLK09, values::YLW09,
                values::RED10, values::BLU10, values::BLK10, values::YLW10,
                values::RED11, values::BLU11, values::BLK11, values::YLW11,
                values::RED12, values::BLU12, values::BLK12, values::YLW12,
                values::RED13, values::BLU13, values::BLK13, values::YLW13];
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::set::Set;

    #[test]
    fn test_minimal_sets() {
        for (nindex, number) in Number::iter().enumerate() {
            let nindex_rev = Number::NUM_NUMBERS - nindex - 1;
            for color in Color::iter() {
                let num_sets_got = Piece::new(number, color).minimal_sets().count();

                let start_sets_missed = (Set::MIN_LEN - 1).saturating_sub(nindex);
                let end_sets_missed = (Set::MIN_LEN - 1).saturating_sub(nindex_rev);
                let num_sets_exp = 1 + Set::MIN_LEN - start_sets_missed - end_sets_missed;

                assert_eq!(num_sets_got, num_sets_exp,
                           "number: {}, start_sets_missed: {}, end_sets_missed: {}",
                           number, start_sets_missed, end_sets_missed);
            }
        }
    }

    #[test]
    fn test_piece_index() {
        let mut i = 0;
        for number in Number::iter() {
            for color in Color::iter() {
                assert_eq!(i, Piece::new(number, color).index());
                i += 1;
            }
        }
    }
}
