use std::array::IntoIter;
use std::convert::AsRef;
use std::fmt::{self, Display};
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};
use rand::seq::SliceRandom;


mod colorset;
pub use colorset::ColorSet;


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Color {
    Red,
    Blue,
    Black,
    Yellow,
}

impl Color {
    pub const NUM_COLORS: usize = 4;

    const NAMES: [&'static str; Color::NUM_COLORS] = ["Red", "Blue", "Black", "Yellow"];
    const MASKS: [u8; Color::NUM_COLORS] = [1, 2, 4, 8];
    const ALL: [Color; Color::NUM_COLORS] = [Color::Red, Color::Blue, Color::Black, Color::Yellow];

    pub fn from_index(index: usize) -> Result<Color, String> {
        match index {
            0 => Ok(Color::Red),
            1 => Ok(Color::Blue),
            2 => Ok(Color::Black),
            3 => Ok(Color::Yellow),
            _ => Err(format!("there is no color for index {}", index)),
        }
    }

    pub fn index(&self) -> usize {
        match self {
            Color::Red => 0,
            Color::Blue => 1,
            Color::Black => 2,
            Color::Yellow => 3,
        }
    }

    pub fn mask(&self) -> u8 {
        Color::MASKS[self.index()]
    }

    pub fn iter() -> IntoIter<Color, { Color::NUM_COLORS }> {
        IntoIter::new(Self::ALL)
    }
}

impl AsRef<u8> for Color {
    fn as_ref(&self) -> &u8 {
        &Color::MASKS[self.index()]
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Color::NAMES[self.index()].fmt(f)
    }
}

impl FromStr for Color {
    type Err = String;

    fn from_str(mut s: &str) -> Result<Color, String> {
        s = s.trim();
        for (color, name) in Color::iter().zip(Color::NAMES.iter()) {
            if name.eq_ignore_ascii_case(s) {
                return Ok(color);
            }
        }
        Err(format!("no color '{}'", s))
    }
}

impl Distribution<Color> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Color {
        *Color::ALL.choose(rng).unwrap()
    }
}


pub mod values {
    use super::*;

    pub const RED: Color = Color::Red;
    pub const BLU: Color = Color::Blue;
    pub const BLK: Color = Color::Black;
    pub const YLW: Color = Color::Yellow;
    pub const ALL: &'static [Color] = &Color::ALL;
}


#[cfg(test)]
mod tests {
    //use super::*;
}
