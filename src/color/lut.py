import numpy as np
from functools import reduce


# defaults
NUM_COPIES = 2
NUM_COLORS = 4
MIN_PIECES = 2
MIN_LEN = 3


def range_inc(a, b=None):
    if b is None:
        b = a
        a = 0
    return range(a, b + 1)


def prod(it):
    return reduce(lambda a, b: a * b, it)


def n_choose_k(n, k):
    if k == 0 or k == n:
        return 1
    return prod(range(k + 1, n + 1)) // prod(range(1, n - k + 1))


def count_colors(colors, jokers, num_copies=NUM_COPIES, num_colors=NUM_COLORS, min_pieces=MIN_PIECES, min_len=MIN_LEN):
    #if colors + jokers > num_colors:
    #    return count_colors(colors, num_colors - colors)

    x = 0
    # -1 everywhere as we pick one color (doesnt matter which one) and count how many sets contain it
    for c in range_inc(min_pieces - 1, colors - 1):
        min_j = max(0, min_len - c - 1)
        max_j = min(num_copies, num_colors - c - 1, jokers)
        x += (max_j - min_j + 1) * n_choose_k(colors - 1, c)
    return x


def count_jokers(colors, jokers, num_copies=NUM_COPIES, num_colors=NUM_COLORS, min_pieces=MIN_PIECES, min_len=MIN_LEN):
    #if colors + jokers > num_colors:
    #    return count_jokers(colors, num_colors - colors)

    x = 0
    for c in range_inc(min_pieces, colors):
        min_j = max(1, min_len - c)  # 1 instead of 0 here because we need at least one joker
        max_j = min(num_copies, num_colors - c, jokers)
        x += (max_j - min_j + 1) * n_choose_k(colors, c)
    return x


def lut(num_copies=NUM_COPIES, num_colors=NUM_COLORS, min_pieces=MIN_PIECES, min_len=MIN_LEN):
    arr = np.zeros((num_colors + 1, num_copies + 1, 2), dtype=int)

    for c in range_inc(num_colors):
        for j in range_inc(num_copies):
            arr[c][j][0] = count_colors(
                    c, j,
                    num_copies=num_copies,
                    num_colors=num_colors,
                    min_pieces=min_pieces,
                    min_len=min_len)
            arr[c][j][1] = count_jokers(
                    c, j,
                    num_copies=num_copies,
                    num_colors=num_colors,
                    min_pieces=min_pieces,
                    min_len=min_len)
    return arr


def read_value(name, default):
    return int(input(f"{name} = [{default}] ") or default)


def print_as_rust_arr(name, arr, name_is_full_decl=False):
    if name_is_full_decl:
        print(f"{name} = [")
    else:
        nrows, ncols = arr.shape
        print(f"static {name.upper()}: [[usize; {ncols}]; {nrows}] = [")
    for row in arr[:-1]:
        print("    [" + ", ".join(str(cell) for cell in row) + "],")
    print("    [" + ", ".join(str(cell) for cell in arr[-1]) + "]];")


if __name__ == "__main__":
    num_copies = read_value("Tile::NUM_COPIES", NUM_COPIES)
    num_colors = read_value("Color::NUM_COLORS", NUM_COLORS)
    min_pieces = read_value("Set::MIN_PIECES", MIN_PIECES)
    min_len = read_value("Set::MIN_LEN", MIN_LEN)

    counts = lut(num_copies=num_copies, num_colors=num_colors, min_pieces=min_pieces, min_len=min_len)
    color_counts = counts[:, :, 0]
    joker_counts = counts[:, :, 1]

    print()
    print_as_rust_arr(
            "const COLOR_COUNTS: [[usize; Tile::NUM_COPIES + 1]; Color::NUM_COLORS + 1]",
            color_counts,
            name_is_full_decl=True)
    print_as_rust_arr(
            "const JOKER_COUNTS: [[usize; Tile::NUM_COPIES + 1]; Color::NUM_COLORS + 1]",
            joker_counts,
            name_is_full_decl=True)
