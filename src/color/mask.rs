pub type Mask = u8;

pub trait ColorMask {
    fn mask(&self) -> Mask;
}
