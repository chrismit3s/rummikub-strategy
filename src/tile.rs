use crate::color::Color;
use crate::number::Number;
use crate::piece::Piece;
use crate::table::TableIndex;
use std::cmp::Ordering;
use std::fmt::{self, Display};
use std::iter::{self, Iterator, IntoIterator, Map};
use std::ops::Range;
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};


pub type TileIter =
    Map<
        Map<
            Range<usize>,
            fn(usize) -> Result<Tile, String>>,
        fn(Result<Tile, String>) -> Tile>;


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Tile {
    Piece(Piece),
    Joker,
}


impl Tile {
    pub const NUM_TILES: usize = Piece::NUM_PIECES + 1;
    pub const MAX_TILES_ON_HAND: usize = 14;
    pub const JOKER_INDEX: usize = Piece::NUM_PIECES;
    pub const JOKER_VALUE: u8 = 30;
    pub const NUM_COPIES: usize = 2;

    const JOKER_NAME: &'static str = "Joker";

    pub fn new(number: Number, color: Color) -> Tile {
        Tile::Piece(Piece { number, color })
    }

    pub fn from_piece(piece: Piece) -> Tile {
        Tile::Piece(piece)
    }

    pub fn from_index(index: usize) -> Result<Tile, String> {
        match index.cmp(&Tile::JOKER_INDEX) {
            Ordering::Greater => Err(format!("There is no tile for index {}", index)),
            Ordering::Equal => Ok(Tile::Joker),
            Ordering::Less => Piece::from_index(index).map(Tile::Piece),
        }
    }

    pub fn value(&self) -> u8 {
        match *self {
            Tile::Piece(piece) => piece.value(),
            Tile::Joker => Tile::JOKER_VALUE,
        }
    }

    pub fn is_joker(&self) -> bool {
        match self {
            Tile::Piece(_) => false,
            Tile::Joker => true,
        }
    }

    pub fn iter() -> TileIter {
        (0..Tile::NUM_TILES)
            .map(Tile::from_index as fn(usize) -> Result<Tile, String>)
            .map(Result::unwrap)
    }
}

impl TableIndex for Tile {
    fn index(&self) -> usize {
        match self {
            Tile::Piece(piece) => piece.index(),
            Tile::Joker => Tile::JOKER_INDEX,
        }
    }
}

impl TableIndex for &Tile {
    fn index(&self) -> usize {
        match self {
            Tile::Piece(piece) => piece.index(),
            Tile::Joker => Tile::JOKER_INDEX,
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Tile::Piece(piece) => piece.fmt(f),
            Tile::Joker => Tile::JOKER_NAME.fmt(f),
        }
    }
}

impl IntoIterator for Tile {
    type Item = Tile;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        iter::once(self)
    }
}

impl FromStr for Tile {
    type Err = String;

    fn from_str(s: &str) -> Result<Tile, String> {
        if Tile::JOKER_NAME.eq_ignore_ascii_case(s) {
            Ok(Tile::Joker)
        }
        else {
            s.parse::<Piece>().map(Tile::from_piece)
        }
    }
}

impl Distribution<Tile> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Tile {
        if rng.gen_ratio(1, Tile::NUM_TILES as u32) {
            Tile::Joker
        }
        else {
            Tile::Piece(rng.gen())
        }
    }
}

pub mod values {
    use super::*;
    use crate::piece::values as piece;

    pub const RED01: Tile = Tile::Piece(piece::RED01);
    pub const RED02: Tile = Tile::Piece(piece::RED02);
    pub const RED03: Tile = Tile::Piece(piece::RED03);
    pub const RED04: Tile = Tile::Piece(piece::RED04);
    pub const RED05: Tile = Tile::Piece(piece::RED05);
    pub const RED06: Tile = Tile::Piece(piece::RED06);
    pub const RED07: Tile = Tile::Piece(piece::RED07);
    pub const RED08: Tile = Tile::Piece(piece::RED08);
    pub const RED09: Tile = Tile::Piece(piece::RED09);
    pub const RED10: Tile = Tile::Piece(piece::RED10);
    pub const RED11: Tile = Tile::Piece(piece::RED11);
    pub const RED12: Tile = Tile::Piece(piece::RED12);
    pub const RED13: Tile = Tile::Piece(piece::RED13);
    pub const BLU01: Tile = Tile::Piece(piece::BLU01);
    pub const BLU02: Tile = Tile::Piece(piece::BLU02);
    pub const BLU03: Tile = Tile::Piece(piece::BLU03);
    pub const BLU04: Tile = Tile::Piece(piece::BLU04);
    pub const BLU05: Tile = Tile::Piece(piece::BLU05);
    pub const BLU06: Tile = Tile::Piece(piece::BLU06);
    pub const BLU07: Tile = Tile::Piece(piece::BLU07);
    pub const BLU08: Tile = Tile::Piece(piece::BLU08);
    pub const BLU09: Tile = Tile::Piece(piece::BLU09);
    pub const BLU10: Tile = Tile::Piece(piece::BLU10);
    pub const BLU11: Tile = Tile::Piece(piece::BLU11);
    pub const BLU12: Tile = Tile::Piece(piece::BLU12);
    pub const BLU13: Tile = Tile::Piece(piece::BLU13);
    pub const BLK01: Tile = Tile::Piece(piece::BLK01);
    pub const BLK02: Tile = Tile::Piece(piece::BLK02);
    pub const BLK03: Tile = Tile::Piece(piece::BLK03);
    pub const BLK04: Tile = Tile::Piece(piece::BLK04);
    pub const BLK05: Tile = Tile::Piece(piece::BLK05);
    pub const BLK06: Tile = Tile::Piece(piece::BLK06);
    pub const BLK07: Tile = Tile::Piece(piece::BLK07);
    pub const BLK08: Tile = Tile::Piece(piece::BLK08);
    pub const BLK09: Tile = Tile::Piece(piece::BLK09);
    pub const BLK10: Tile = Tile::Piece(piece::BLK10);
    pub const BLK11: Tile = Tile::Piece(piece::BLK11);
    pub const BLK12: Tile = Tile::Piece(piece::BLK12);
    pub const BLK13: Tile = Tile::Piece(piece::BLK13);
    pub const YLW01: Tile = Tile::Piece(piece::YLW01);
    pub const YLW02: Tile = Tile::Piece(piece::YLW02);
    pub const YLW03: Tile = Tile::Piece(piece::YLW03);
    pub const YLW04: Tile = Tile::Piece(piece::YLW04);
    pub const YLW05: Tile = Tile::Piece(piece::YLW05);
    pub const YLW06: Tile = Tile::Piece(piece::YLW06);
    pub const YLW07: Tile = Tile::Piece(piece::YLW07);
    pub const YLW08: Tile = Tile::Piece(piece::YLW08);
    pub const YLW09: Tile = Tile::Piece(piece::YLW09);
    pub const YLW10: Tile = Tile::Piece(piece::YLW10);
    pub const YLW11: Tile = Tile::Piece(piece::YLW11);
    pub const YLW12: Tile = Tile::Piece(piece::YLW12);
    pub const YLW13: Tile = Tile::Piece(piece::YLW13);
    pub const JOKER: Tile = Tile::Joker;

    pub const ALL: &'static [Tile] = &[
                values::RED01, values::BLU01, values::BLK01, values::YLW01,
                values::RED02, values::BLU02, values::BLK02, values::YLW02,
                values::RED03, values::BLU03, values::BLK03, values::YLW03,
                values::RED04, values::BLU04, values::BLK04, values::YLW04,
                values::RED05, values::BLU05, values::BLK05, values::YLW05,
                values::RED06, values::BLU06, values::BLK06, values::YLW06,
                values::RED07, values::BLU07, values::BLK07, values::YLW07,
                values::RED08, values::BLU08, values::BLK08, values::YLW08,
                values::RED09, values::BLU09, values::BLK09, values::YLW09,
                values::RED10, values::BLU10, values::BLK10, values::YLW10,
                values::RED11, values::BLU11, values::BLK11, values::YLW11,
                values::RED12, values::BLU12, values::BLK12, values::YLW12,
                values::RED13, values::BLU13, values::BLK13, values::YLW13,
                values::JOKER];
}


#[cfg(test)]
mod tests {
    //use super::*;
}
