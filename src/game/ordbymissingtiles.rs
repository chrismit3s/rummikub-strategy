use crate::set::Set;
use crate::table::SetTable;
use std::cmp::Ordering;
use std::fmt::{self, Display, Debug};


#[derive(Clone)]
pub struct OrdByMissingTiles {
    set: Set,
    missing_tiles: usize,
}

impl OrdByMissingTiles {
    pub fn new(set: Set, table: &SetTable) -> OrdByMissingTiles {
        OrdByMissingTiles {
            set,
            missing_tiles: table.tiles_missing_for(&set),
        }
    }

    pub fn set(&self) -> &Set {
        &self.set
    }

    pub fn missing_tiles(&self) -> usize {
        self.missing_tiles
    }
}

impl Display for OrdByMissingTiles {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "OrdByMissingTiles({} -> {} missing)", self.set, self.missing_tiles)
    }
}

impl Debug for OrdByMissingTiles {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl PartialEq for OrdByMissingTiles {
    fn eq(&self, other: &Self) -> bool {
        self.missing_tiles == other.missing_tiles
    }
}

impl Eq for OrdByMissingTiles {}

impl PartialOrd for OrdByMissingTiles {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // other first so we reverse the order, the less tiles are missing, the bigger self is
        other.missing_tiles.partial_cmp(&self.missing_tiles)
    }
}

impl Ord for OrdByMissingTiles {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}
