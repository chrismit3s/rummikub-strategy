use crate::game::{Adapter, Command, Action};
use crate::tile::Tile;
use crate::table::{TileTable, Table};
use crate::set::Set;
use std::io::{self, Write};
use std::iter;


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Prompt {
    adapter: Adapter,
    done: bool,
}


impl Prompt {
    const PROMPT: &'static str = "? ";

    pub fn new() -> Prompt {
        Prompt { adapter: Adapter::new(), done: false }
    }

    pub fn running() {
        Prompt::new().run()
    }

    pub fn run(&mut self) {
        let mut action;
        while !self.done {
            action = self.handle(self.command(self.prompt()));
            self.act(action);
        }
    }

    pub fn act(&mut self, action: Action) {
        match action {
            Action::Noop => (),
            Action::Print => println!("{}", self.adapter.game),
            Action::Exit => self.done = true,
            Action::Error { msg } => eprintln!("Error: {}", msg),
            Action::Exported { savegame } => println!("Exported: {}", savegame),
            Action::BestMeld { sets, tiles_played } => {
                if tiles_played.iter_tile_values().all(|(_t, c)| c == 0) {
                    println!("No meld possible");
                    return;
                }

                let num_jokers = tiles_played[Tile::Joker];
                if num_jokers < 0 {
                    println!("Jokers gained: {}", -num_jokers);
                }
                println!("Best tiles to play: {} [{:X}]",
                         tiles_played
                            .iter_tile_values()
                            .filter(|&(_t, v)| v > 0)
                            .flat_map(|(t, v)| iter::repeat(t).take(v as usize))
                            .map(|t| t.to_string())
                            .collect::<Vec<_>>()
                            .join(", "),
                         tiles_played);
                println!("Set on board: {}", sets.into_iter()
                                                    .flat_map(|(s, c)| iter::repeat(s).take(c))
                                                    .map(|s| s.to_string())
                                                    .collect::<Vec<_>>()
                                                    .join(", "));
            },
            Action::BestDrop { tile } => println!("Best tile to drop: {}", tile),
        }
    }

    pub fn handle(&mut self, command: Result<Command, String>) -> Action {
        match command {
            Ok(cmd) => self.adapter.handle(cmd),
            Err(err) => Action::Error { msg: err },
        }
    }

    pub fn prompt(&self) -> String {
        let stdout = io::stdout();
        let mut stdout = stdout.lock();
        write!(stdout, "{}", Self::PROMPT).unwrap();
        stdout.flush().unwrap();
        let mut line = String::new();
        io::stdin().read_line(&mut line).unwrap();
        line
    }

    pub fn command(&self, mut line: String) -> Result<Command, String> {
        line.make_ascii_lowercase();
        let mut args = line.split_whitespace();
        match args.next() {
            Some("best_meld") => Self::is_first_meld(args).map(|is_first_meld| Command::BestMeld { is_first_meld }),
            Some("best_drop") => Self::is_first_meld(args).map(|is_first_meld| Command::BestDrop { is_first_meld }),

            Some("draw") => Self::tile(args).map(|tile| Command::DrawTile { tile }),
            Some("drop") => Self::tile(args).map(|tile| Command::DropTile { tile }),
            Some("meld") => Self::tiletable(args).map(|tiletable| Command::MeldTiles { tiletable }),

            Some("hand") => Self::tiles(args).map(|(t, multiplier)| Command::ModifyHand { tiles: t, multiplier }),
            Some("board") => Self::tiles(args).map(|(t, multiplier)| Command::ModifyBoard { tiles: t, multiplier }),

            Some("export") => Self::no_args(args).map(|()| Command::Export),
            Some("import") => Self::savegame(args).map(|savegame| Command::Import { savegame }),

            Some("print") => Self::no_args(args).map(|()| Command::Print),
            Some("quit") => Self::no_args(args).map(|()| Command::Exit),
            Some("exit") => Self::no_args(args).map(|()| Command::Exit),
            Some(arg) => Err(Self::invalid_arg("cmd", arg, None)),
            None => Err(Self::missing_arg("cmd")),
        }
    }

    fn is_first_meld<'a>(mut args: impl Iterator<Item = &'a str>) -> Result<bool, String> {
        let is_first_meld = match args.next() {
            Some(arg) => arg.parse::<bool>().map_err(|_| Self::invalid_arg("is_first_meld", arg, Some("cannot parse bool")))?,
            None => false,
        };

        Self::no_args(args).map(|()| is_first_meld)
    }

    fn tiles<'a>(mut args: impl Iterator<Item = &'a str>) -> Result<(Vec<Tile>, isize), String> {
        let multiplier = match args.next() {
            Some("add") => 1,
            Some("remove") => -1,
            Some(arg) => match arg.parse::<isize>() {
                Ok(mult) => mult,
                Err(_) => return Err(Self::invalid_arg("multiplier", arg, Some("cannot parse int"))),
            },
            None => return Err(Self::missing_arg("multiplier")),
        };

        let tiles = match args.next() {
            Some(tiles) => tiles,
            None => return Err(Self::missing_arg("tiles")),
        };
        let tiles = match (tiles.parse::<Tile>(), tiles.parse::<Set>()) {
            (Ok(tile), _) => vec![tile],
            (_, Ok(set)) => set.into_iter().collect(),
            (Err(tile_err), Err(set_err)) => return Err(Self::invalid_arg("tiles", tiles, Some(&format!("{}, {}", tile_err, set_err)))),
        };

        Self::no_args(args).map(|()| (tiles, multiplier))
    }

    fn tile<'a>(mut args: impl Iterator<Item = &'a str>) -> Result<Tile, String> {
        let tile = args.next().ok_or_else(|| Self::missing_arg("tile"))?.parse()?;
        Self::no_args(args).map(|()| tile)
    }

    fn tiletable<'a>(mut args: impl Iterator<Item = &'a str>) -> Result<TileTable<isize>, String> {
        let tiletable = args.next().ok_or_else(|| Self::missing_arg("tiletable"))?.parse()?;
        Self::no_args(args).map(|()| tiletable)
    }

    fn savegame<'a>(mut args: impl Iterator<Item = &'a str>) -> Result<String, String> {
        let savegame = match args.next() {
            Some(arg) => String::from(arg),
            None => return Err(Self::missing_arg("savegame")),
        };
        Self::no_args(args).map(|()| savegame)
    }

    fn no_args<'a>(mut args: impl Iterator<Item = &'a str>) -> Result<(), String> {
        match args.next() {
            None => Ok(()),
            Some(arg) => Err(format!("Unknown argument: '{}'", arg)),
        }
    }

    fn invalid_arg(name: &str, arg: &str, err: Option<&str>) -> String {
        match err {
            Some(err) => format!("Invalid argument for <{}>: '{}' ({})", name, arg, err),
            None => format!("Invalid argument for <{}>: '{}'", name, arg),
        }
    }

    fn missing_arg(name: &str) -> String {
        format!("Missing argument <{}>", name)
    }
}
