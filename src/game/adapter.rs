use crate::game::Game;
use crate::set::Set;
use crate::table::{TileTable, Table};
use crate::tile::Tile;
use std::collections::HashMap;


#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Command {
    BestMeld { is_first_meld: bool },
    BestDrop { is_first_meld: bool },

    DrawTile { tile: Tile },
    DropTile { tile: Tile },
    MeldTiles { tiletable: TileTable<isize> },

    ModifyHand { tiles: Vec<Tile>, multiplier: isize },
    ModifyBoard { tiles: Vec<Tile>, multiplier: isize },

    Import { savegame: String },
    Export,

    Print,
    Clear,
    Exit,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Action {
    BestMeld { sets: HashMap<Set, usize>, tiles_played: TileTable<isize> },
    BestDrop { tile: Tile },

    Exported { savegame: String },

    Error { msg: String },
    Print,
    Noop,
    Exit,
}


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Adapter {
    pub game: Game,
}


impl Adapter {
    pub fn new() -> Adapter {
        Adapter { game: Game::new() }
    }

    pub fn handle(&mut self, cmd: Command) -> Action {
        match cmd {
            Command::BestMeld { is_first_meld } => self.beld_meld(is_first_meld),
            Command::BestDrop { is_first_meld } => self.beld_drop(is_first_meld),

            Command::DrawTile { tile } => self.draw_tile(tile),
            Command::DropTile { tile } => self.drop_tile(tile),
            Command::MeldTiles { tiletable } => self.meld_tiles(tiletable),

            Command::ModifyHand { tiles, multiplier } => self.modify_hand(tiles, multiplier),
            Command::ModifyBoard { tiles, multiplier } => self.modify_board(tiles, multiplier),

            Command::Import { savegame } => self.import(savegame),
            Command::Export => self.export(),

            Command::Clear => self.clear(),
            Command::Print => self.print(),
            Command::Exit => self.exit(),
        }
    }

    fn best_meld(&mut self, is_first_meld: bool) -> Action {
        match self.game.best_meld(is_first_meld) {
            Some((sets, tiles_played)) => Action::BestMeld { sets, tiles_played },
            None => Action::Error { msg: String::from("No meld possible") },
        }
    }

    fn best_drop(&mut self, is_first_meld: bool) -> Action {
        match self.game.best_drop(is_first_meld) {
            Some(tile) => Action::BestDrop { tile },
            None => Action::Error { msg: String::from("No tile to drop") },
        }
    }

    fn draw_tile(&mut self, tile: Tile) -> Action {
        match self.game.draw_tile(tile) {
            Ok(()) => Action::Print,
            Err(msg) => Action::Error { msg },
        }
    }

    fn drop_tile(&mut self, tile: Tile) -> Action {
        match self.game.drop_tile(tile) {
            Ok(()) => Action::Print,
            Err(msg) => Action::Error { msg },
        }
    }

    fn meld_tiles(&mut self, tiletable: TileTable<isize>) -> Action {
        match self.game.meld_tiles(tiletable) {
            Ok(()) => Action::Print,
            Err(msg) => Action::Error { msg },
        }
    }

    fn modify_hand(&mut self, tiles: Vec<Tile>, multiplier: isize) -> Action {
        match self.game.change_hand(tiles, multiplier) {
            Ok(_) => Action::Print,
            Err(msg) => Action::Error { msg },
        }
    }

    fn modify_board(&mut self, tiles: Vec<Tile>, multiplier: isize) -> Action {
        match self.game.change_board(tiles, multiplier) {
            Ok(_) => Action::Print,
            Err(msg) => Action::Error { msg },
        }
    }

    fn import(&mut self, savegame: String) -> Action {
        match savegame.parse() {
            Ok(g) => { self.game = g; Action::Print }
            Err(msg) => Action::Error { msg },
        }
    }

    fn export(&self) -> Action {
        Action::Exported { savegame: format!("{:x}", self.game) }
    }

    fn clear(&mut self) -> Action {
        self.game.hand.clear();
        self.game.board.clear();
        Action::Print
    }

    fn print(&self) -> Action {
        Action::Print
    }

    fn exit(&self) -> Action {
        Action::Exit
    }
}
