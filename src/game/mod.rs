use crate::set::Set;
use crate::table::{TileTable, SetTable, Table, TableIndex};
use crate::tile::Tile;
use std::cmp::Ordering;
use std::collections::{HashMap, BinaryHeap};
use std::fmt::{self, Display, UpperHex, LowerHex, Debug};
use std::iter::{self, Iterator};
use std::str::FromStr;
use itertools::Itertools;
use rand::Rng;
use rand::distributions::{Distribution, Standard};
use rand::seq::IteratorRandom;


mod ordbymissingtiles;
pub use ordbymissingtiles::OrdByMissingTiles;

mod adapter;
pub use adapter::{Adapter, Command, Action};

mod prompt;
pub use prompt::Prompt;


fn mins_by_key<'a, E, K, F, I>(i: I, key: F) -> impl Iterator<Item = (K, Vec<E>)> + 'a
        where E: Clone + Ord + 'a, I: Iterator<Item = E> + 'a, K: Clone + PartialEq + 'a, F: Fn(&E) -> K + 'a {
    let mut heap: BinaryHeap<_> = i.collect();
    let sorted = iter::from_fn(move || heap.pop());
    //sorted.group_by(key).into_iter().map(|(k, g)| (k, g.collect()))  // temporary value created
    sorted.batching(move |sorted_| sorted_.next().map(|x| {
        let k = (key)(&x);
        (k.clone(), iter::once(x).chain(sorted_.take_while_ref(|y| (key)(y) == k)).collect())
    }))
}


#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Game {
    total: SetTable,  // hand + board
    board: SetTable,
    hand: SetTable,
}

impl Game {
    fn get_change_func<T: TableIndex>(v: isize)
            -> (for<'a> fn(&'a mut SetTable, T, usize) -> Result<(), String>, usize) {
        // returns the function used to change a settable and the corresponding parameter
        // all that to be able to handle isize with add(usize) and remove(usize)
        (if v < 0 { SetTable::remove_tile::<T> } else { SetTable::add_tile::<T> }, v.unsigned_abs())
    }

    fn change<T, I>(i: I, table_a: &mut SetTable, table_b: &mut SetTable, value_a: isize, value_b: isize)
            -> Result<(), String>
            where T: TableIndex, I: IntoIterator<Item = T> {
        let (func_a, na) = Game::get_change_func::<T>(value_a);
        let (func_b, nb) = Game::get_change_func::<T>(value_b);

        i.into_iter().try_for_each(|t| {
            (func_a)(table_a, t, na)?;
            (func_b)(table_b, t, nb)?;
            Ok(())
        })
    }

    pub fn new() -> Game {
        Game {
            hand: SetTable::empty(),
            board: SetTable::empty(),
            total: SetTable::empty(),
        }
    }

    pub fn from(hand: SetTable, board: SetTable) -> Game {
        Game { hand, board, total: SetTable::from_tiles(hand.iter_tile_values().chain(board.iter_tile_values())).unwrap() }
    }

    pub fn change_hand<T: TableIndex, I: IntoIterator<Item = T>>(&mut self, i: I, v: isize) -> Result<(), String> {
        Game::change(i, &mut self.hand, &mut self.total, v, v)
    }

    pub fn change_board<T: TableIndex, I: IntoIterator<Item = T>>(&mut self, i: I, v: isize) -> Result<(), String> {
        Game::change(i, &mut self.board, &mut self.total, v, v)
    }

    pub fn draw_tile(&mut self, t: Tile) -> Result<(), String> {
        self.change_hand(t, 1)
    }

    pub fn drop_tile(&mut self, t: Tile) -> Result<(), String> {
        self.change_hand(t, -1)  // TODO impl in adapter/prompt
    }

    pub fn meld_tiles(&mut self, t: TileTable<isize>) -> Result<(), String> {
        t.iter_tile_values()
            .try_for_each(|(t, v)| Game::change(t, &mut self.hand, &mut self.board, -v, v))
    }

    pub fn tiles_on_hand(&self) -> usize {
        self.hand.len()
    }

    pub fn best_meld(&mut self, is_first_meld: bool) -> Option<(HashMap<Set, usize>, TileTable<isize>)> {
        let jokers_on_board = if is_first_meld { 0 } else { self.board.num_jokers() };
        let mut skippable = self.hand.tiles().clone();
        skippable.add_tile(Tile::Joker, jokers_on_board).unwrap();

        let table = if is_first_meld { &mut self.hand } else { &mut self.total };
        table.is_possible_with_optional(&mut skippable)
            .map(|(sets, skips, _skipped_value)|
                 (sets, TileTable::from_tiles(skippable.iter_tile_values()
                                                    .zip(skips.into_iter())
                                                    .map(|((t, a), b)| (t, a as isize - b as isize))).unwrap()))
    }

    pub fn best_drop(&self, is_first_meld: bool) -> Option<Tile> {
        let table = if is_first_meld { &self.hand } else { &self.total };

        let mut scores = TileTable::<f32>::empty();

        // the way we figure out which tile is the best to drop, we let each tile "vote",
        // every tile has one vote, which can be split any way (1/2 + 1/2, 1/2 + 1/3 + 1/6)
        // and it votes for the best sets it could be a part of
        for (piece, piece_count) in table.iter_piece_values().filter(|&(_, i)| i != 0) {
            let mut set_count = 0;
            for (_, sets) in
                     mins_by_key(piece.minimal_sets().map(|set| OrdByMissingTiles::new(set, &table)),
                                 OrdByMissingTiles::missing_tiles) {
                println!("{} {:?}", piece, &sets);
                let num_sets = sets.len();
                let score_inc = piece_count as f32 / num_sets as f32;

                sets.into_iter()
                        .flat_map(|set| set.set().into_iter())
                        .for_each(|tile| scores[tile] += score_inc);

                set_count += num_sets;
                if set_count >= piece_count {
                    break;
                }
            }
        }

        // drop tile with the least amount of votes
        println!("{}", scores);
        self.hand.iter_piece_values()
            .zip(scores.into_iter())
            .filter(|&((_p, i), _s)| i != 0)
            .min_by(|(_, s1), (_, s2)| s1.partial_cmp(s2).unwrap_or(Ordering::Equal))
            .map(|((p, _i), _s)| Tile::from_piece(p))
    }
}

impl Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Board:{:.6}", self.board)?;  // the .6 stands for the 6 chars in "Board:"
        write!(f, "Hand:{:.5}", self.hand)?;
        Ok(())
    }
}

impl Debug for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Board:{:.6}", self.board)?;  // the .6 stands for the 6 chars in "Board:"
        write!(f, "Hand:{:.5}", self.hand)?;
        write!(f, "Total:{:.6}", self.total)?;
        Ok(())
    }
}

impl LowerHex for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:x}:{:x}", self.board, self.hand)
    }
}

impl UpperHex for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:X}:{:X}", self.board, self.hand)
    }
}

impl FromStr for Game {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, String> {
        let mut iter = s.split(":");
        let board: SetTable = iter.next()
                    .ok_or(String::from("No board hex string specified"))?
                    .parse().map_err(|_| String::from("Invalid board hex specified"))?;
        let hand: SetTable = iter.next()
                    .ok_or(String::from("No hand hex string specified"))?
                    .parse().map_err(|_| String::from("Invalid hand hex specified"))?;

        Ok(Game::from(hand, board))
    }
}

impl Distribution<Game> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Game {
        let board: SetTable = rng.gen();

        let num_tiles = rng.gen_range(0..=Tile::MAX_TILES_ON_HAND);
        let hand = SetTable::from_slice(&board.iter_tile_values()
                                            .flat_map(|(t, v)| iter::repeat(t).take(Tile::NUM_COPIES - v))
                                            .choose_multiple(rng, num_tiles)).unwrap();

        Game::from(hand, board)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    extern crate test;

    use crate::color::{ColorSet, values as colors};
    use crate::number::{NumberSet, values as numbers};
    use crate::tile::values as tiles;
    use test::Bencher;
    use rand::random;
    use rand_pcg::Pcg64Mcg;

    #[test]
    fn test_mins_by_key() {
        let rep = 3;
        let max = 5;
        let iter = (0..max).map(|i| (i, i.to_string())).flat_map(|t| iter::repeat(t).take(rep)).inspect(|x| println!("{:?}", x));
        for ((k, ts), i) in mins_by_key(iter, |t| max - t.0 - 1).zip(0..5) {
            println!("i={}, k={}, ts={:?}", i, k, ts);
            assert_eq!(k, i, "key");
            assert_eq!(ts.len(), rep, "len");
        }
    }

    #[test]
    fn test_best_drop() {
        let mut g = Game::new();
        g.change_board(Set::group(numbers::N04, ColorSet::from_mask(0b0111), 0).unwrap(), 1).unwrap();
        g.change_board(Set::group(numbers::N05, ColorSet::from_mask(0b0111), 0).unwrap(), 1).unwrap();
        g.change_board(Set::group(numbers::N06, ColorSet::from_mask(0b1110), 0).unwrap(), 1).unwrap();
        g.change_board(Set::run(NumberSet::from_run(7, 4), colors::RED, 0).unwrap(), 1).unwrap();

        g.change_hand(Set::run(NumberSet::from_run(0, 3), colors::BLK, 0).unwrap(), 1).unwrap();

        let bad_tile = tiles::BLK12;
        g.change_hand(bad_tile, 1).unwrap();
        assert_eq!(g.best_drop(false), Some(bad_tile));
    }

    #[test]
    fn test_best_meld() {
        let mut g = Game::new();
        g.change_board(Set::group(numbers::N03, ColorSet::from_mask(0b0111), 0).unwrap(), 1).unwrap();
        g.change_board(Set::group(numbers::N04, ColorSet::from_mask(0b0111), 0).unwrap(), 1).unwrap();
        g.change_board(Set::group(numbers::N05, ColorSet::from_mask(0b0111), 0).unwrap(), 1).unwrap();

        g.change_hand(tiles::RED02, 1).unwrap();
        println!("{}", g);
        if let Some((sets, tiles)) = g.best_meld(false) {
            assert_eq!(tiles.total(), 1, "\n{}", tiles);
            assert_eq!(sets.len(), 3, "{:?}", sets);
        }
        else {
            panic!("best_meld returned none\nGame:{:.5}", g);
        }

        g.change_hand(tiles::BLU06, 1).unwrap();
        println!("{}", g);
        if let Some((sets, tiles)) = g.best_meld(false) {
            assert_eq!(tiles.total(), 2, "\n{}", tiles);
            assert_eq!(sets.len(), 3, "{:?}", sets);
        }
        else {
            panic!("best_meld returned none\nGame:{:.5}", g);
        }

        g.change_hand(tiles::BLU06, -1).unwrap();
        g.change_hand(tiles::YLW06, 1).unwrap();
        println!("{}", g);
        if let Some((sets, tiles)) = g.best_meld(false) {
            assert_eq!(tiles.total(), 1, "\n{}", tiles);
            assert_eq!(sets.len(), 3, "{:?}", sets);
        }
        else {
            panic!("best_meld returned none\nGame:{:.5}", g);
        }
    }

    #[test]
    fn test_to_from_hex_fixpoint() {
        for _ in 0..32 {
            let game: Game = random();
            assert_eq!(Ok(game), format!("{:x}", game).parse());
        }
    }

    #[bench]
    fn bench_best_meld(b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(0xBADF00DBABE);
        b.iter(move || {
            let mut game: Game = rng.gen();
            game.best_meld(false)
        })
    }

    #[bench]
    fn bench_best_drop(b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(0xBADF00DBABE);
        b.iter(move || {
            let game: Game = rng.gen();
            game.best_drop(false)
        })
    }

    fn draw_tile<R: Rng + ?Sized>(rng: &mut R, game: &mut Game, tiles: &mut TileTable<usize>) {
        if tiles.len() == 0 {
            tiles.add_tiles(Tile::iter().map(|t| (t, Tile::NUM_COPIES))).unwrap();
        }

        let tile = tiles.iter_tiles().choose(rng).unwrap();
        tiles.remove_tile(tile, 1).unwrap();
        game.draw_tile(tile).unwrap();
    }

    fn play_game<R: Rng + ?Sized>(rng: &mut R) {
        let mut game = Game::new();
        let mut is_first_meld = true;
        let mut tile_pot = TileTable::<usize>::empty();

        // setup
        (0..Tile::MAX_TILES_ON_HAND).for_each(|_| draw_tile(rng, &mut game, &mut tile_pot));

        while game.tiles_on_hand() > 0 {
            draw_tile(rng, &mut game, &mut tile_pot);
            if let Some((_, tiles)) = game.best_meld(is_first_meld) {
                is_first_meld = false;
                game.meld_tiles(tiles).unwrap();
            }
            else {
                game.drop_tile(game.best_drop(is_first_meld).unwrap()).unwrap();
            }
        }

    }

    #[bench]
    fn bench_play_game(b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(0xBADF00DBABE);

        b.iter(move || play_game(&mut rng))
    }
}
