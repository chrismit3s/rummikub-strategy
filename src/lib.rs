#![feature(min_type_alias_impl_trait)]
#![feature(never_type)]
#![feature(test)]
pub mod color;
pub mod game;
pub mod number;
pub mod piece;
pub mod set;
pub mod table;
pub mod tile;
