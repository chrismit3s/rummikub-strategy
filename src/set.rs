use crate::color::{Color, ColorSet, values as colors};
use crate::number::{Number, NumberSet, values as numbers};
use crate::tile::Tile;
use crate::piece::Piece;
use std::fmt::{self, Display};
use std::iter::{self, IntoIterator, Iterator};
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};
use rand::seq::SliceRandom;


#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Set {
    Group { number: Number, colors: ColorSet, num_jokers: usize },
    Run { numbers: NumberSet, color: Color, num_jokers: usize },
}


impl Set {
    pub const MIN_LEN: usize = 3;
    pub const MIN_PIECES: usize = 2;
    pub const FIRST_MELD_MIN_VALUE: u8 = 30;

    pub fn check_set(set: &Set) -> Result<(), String> {
        let len = set.len();
        let num_jokers = set.num_jokers();
        if len < Set::MIN_LEN {
            Err(format!("Too few tiles for {} (got: {}, need at least: {})", set, len, Set::MIN_LEN))
        }
        else if num_jokers > Tile::NUM_COPIES {
            Err(format!("Too many jokers for {} (got: {}, maximum allowed: {})", set, num_jokers, Tile::NUM_COPIES))
        }
        else if num_jokers == Tile::NUM_COPIES && len == Set::MIN_LEN {
            Err(format!("Set {} could be a group or a run", set))
        }
        else {
            Ok(())
        }
    }

    pub fn group(number: Number, colors: ColorSet, num_jokers: usize) -> Result<Set, String> {
        let set = Set::Group { number, colors, num_jokers };
        Set::check_set(&set)?;
        Ok(set)
    }

    pub fn run(numbers: NumberSet, color: Color, num_jokers: usize) -> Result<Set, String> {
        let set = Set::Run { numbers, color, num_jokers };
        Set::check_set(&set)?;
        Ok(set)
    }

    pub fn is_group(&self) -> bool {
        match self {
            Set::Group { number: _, colors: _, num_jokers: _ } => true,
            Set::Run { numbers: _, color: _, num_jokers: _ } => false,
        }
    }

    pub fn is_run(&self) -> bool {
        !self.is_group()
    }

    pub fn num_jokers(&self) -> usize {
        match self {
            Set::Group { number: _, colors: _, num_jokers } => *num_jokers,
            Set::Run { numbers: _, color: _, num_jokers } => *num_jokers,
        }
    }

    pub fn contains(&self, tile: &Tile) -> bool {
        match self {
            Set::Group { number: group_number, colors: group_colors, num_jokers } => match tile {
                Tile::Joker => *num_jokers > 0,
                Tile::Piece(Piece { number: tile_number, color: tile_color })
                    => tile_number == group_number && group_colors.contains(*tile_color),
            },
            Set::Run { numbers: run_numbers, color: run_color, num_jokers } => match tile {
                Tile::Joker => *num_jokers > 0,
                Tile::Piece(Piece { number: tile_number, color: tile_color })
                    => run_color == tile_color && run_numbers.contains(*tile_number),
            },
        }
    }

    pub fn value(&self) -> u8 {
        match *self {
            Set::Group { number, colors, num_jokers } => (colors.len() + num_jokers) as u8 * number.value(),
            Set::Run { numbers, color: _, num_jokers } => {
                let low = numbers.lowest().unwrap();
                let high = numbers.highest().unwrap();  // inclusive
                let jokers = num_jokers - numbers.jokers_needed_for(low.index(), high.index() - low.index() + 1);

                let from = low.value();
                let to = high.value() + jokers as u8;  // value as high as possible

                ((to + 1) * to - from * (from - 1)) / 2
            },
        }
    }

    pub fn len(&self) -> usize {
        match *self {
            Set::Group { number: _, colors, num_jokers } => colors.len() + num_jokers,
            Set::Run { numbers, color: _, num_jokers } => numbers.len() + num_jokers,
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = Tile> + '_ {
        let outer_num_jokers;
        let outer_number;
        let outer_numbers;
        let outer_color;
        let outer_colors;

        match *self {
            Set::Group { number, colors, num_jokers } => {
                outer_num_jokers = num_jokers;
                outer_number = number;
                outer_colors = colors;

                outer_numbers = NumberSet::empty();
                outer_color = Color::Black;  // will be ignored as outer_numbers is emtpy
            },
            Set::Run { numbers, color, num_jokers } => {
                outer_num_jokers = num_jokers;
                outer_numbers = numbers;
                outer_color = color;

                outer_colors = ColorSet::empty();
                outer_number = Number::from_value(1).unwrap();  // will be ignored as outer_colors is emtpy
            }
        }

        iter::repeat(Tile::Joker)
                .take(outer_num_jokers)
                .chain(outer_numbers.into_iter()
                    .map(move |n| Tile::new(n, outer_color)))
                .chain(outer_colors.into_iter()
                    .map(move |c| Tile::new(outer_number, c)))
    }
}

impl Display for Set {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Set::Group { number, colors, num_jokers } if *num_jokers == 0
                => write!(f, "Group@{}[{}]", number, colors),
            Set::Group { number, colors, num_jokers }
                => write!(f, "Group@{}[{}]+{}J", number, colors, num_jokers),

            Set::Run { numbers, color, num_jokers } if *num_jokers == 0
                => write!(f, "Run@{}[{}]", color, numbers),
            Set::Run { numbers, color, num_jokers }
                => write!(f, "Run@{}[{}]+{}J", color, numbers, num_jokers),
        }
    }
}

impl FromStr for Set {
    type Err = String;

    fn from_str(s: &str) -> Result<Set, String> {
        match s.split_once("@") {
            Some((set_type, s)) => {
                let (set_spec, joker_spec) = s.split_once("+").unwrap_or((s, "0j"));
                let num_jokers = match joker_spec.strip_suffix("j").map(|s| s.parse::<usize>()) {
                    Some(Ok(n)) => n,
                    Some(Err(e)) => return Err(format!("invalid joker spec '{}' ({})", joker_spec, e)),
                    None => return Err(format!("'j' missing in joker spec '{}'", joker_spec)),
                };

                // get this single_spec (specifies a single number/color) after the @ and the
                // multispec (specifies a number/color set) in the []
                let (single_spec, multi_spec) = set_spec.split_once("[")
                                                             .ok_or(format!("'[' missing in set spec '{}'", set_spec))?;
                let multi_spec = multi_spec.strip_suffix("]").ok_or(format!("']' missing in set spec '{}'", set_spec))?;

                match set_type {
                    "run" => Set::run(multi_spec.parse()?, single_spec.parse()?, num_jokers),
                    "group" => Set::group(single_spec.parse()?, multi_spec.parse()?, num_jokers),
                    _ => Err(format!("invalid set type '{}'", set_type)),
                }
            },
            None => Err(format!("'@' missing in set spec '{}'", s)),
        }
    }
}

impl IntoIterator for Set {
    type Item = Tile;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        let outer_num_jokers;
        let outer_number;
        let outer_numbers;
        let outer_color;
        let outer_colors;

        match self {
            Set::Group { number, colors, num_jokers } => {
                outer_num_jokers = num_jokers;
                outer_number = number;
                outer_colors = colors;

                outer_numbers = NumberSet::empty();
                outer_color = Color::Black;  // will be ignored as outer_numbers is emtpy
            },
            Set::Run { numbers, color, num_jokers } => {
                outer_num_jokers = num_jokers;
                outer_numbers = numbers;
                outer_color = color;

                outer_colors = ColorSet::empty();
                outer_number = Number::from_value(1).unwrap();  // will be ignored as outer_colors is emtpy
            }
        }

        iter::repeat(Tile::Joker)
                .take(outer_num_jokers)
                .chain(outer_numbers.into_iter()
                    .map(move |n| Tile::new(n, outer_color)))
                .chain(outer_colors.into_iter()
                    .map(move |c| Tile::new(outer_number, c)))
    }
}

impl Distribution<Set> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Set {
        let num_jokers = rng.gen_range(0..=Tile::NUM_COPIES);
        if rng.gen() {  // group
            let num_colors_min = Set::MIN_PIECES.max(Set::MIN_LEN - num_jokers);
            let num_colors_max = Color::NUM_COLORS - num_jokers;
            let num_colors = rng.gen_range(num_colors_min..=num_colors_max);
            let colorset = ColorSet::from_iter(colors::ALL.choose_multiple(rng, num_colors).copied());

            Set::group(rng.gen(), colorset, num_jokers).unwrap()
        }
        else {  // run
            let start = rng.gen_range(0..(Number::NUM_NUMBERS - Set::MIN_LEN));
            let end = rng.gen_range((start + Set::MIN_LEN)..Number::NUM_NUMBERS);
            let length = end - start;

            let num_numbers_min = Set::MIN_PIECES.max(length - num_jokers);
            let num_numbers_max = length - num_jokers.saturating_sub(start + Number::NUM_NUMBERS - end);
            let num_numbers = rng.gen_range(num_numbers_min..=num_numbers_max);
            let numberset = NumberSet::from_iter(numbers::ALL[start..(start + length)].choose_multiple(rng, num_numbers).copied());

            Set::run(numberset, rng.gen(), num_jokers).unwrap()
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    use rand::random;

    #[test]
    fn test_value_no_jokers() {
        for _ in 0..32 {
            let set: Set = random();
            if set.num_jokers() == 0 {
                assert_eq!(set.value(), set.iter().map(|t| t.value()).sum());
            }
        }
    }
}
