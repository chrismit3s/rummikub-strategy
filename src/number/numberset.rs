use crate::number::{Number, values as numbers};
use crate::set::Set;
use std::borrow::Borrow;
use std::convert::{TryInto, identity};
use std::fmt::{self, Display};
use std::iter::{self, DoubleEndedIterator, IntoIterator, Iterator};
use std::ops::{AddAssign, SubAssign, Index};
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct NumberSet {
    accum: [usize; Number::NUM_NUMBERS],
}


impl NumberSet {
    const SEPARATOR: &'static str = ",";

    pub fn empty() -> NumberSet {
        NumberSet {
            accum: [0; Number::NUM_NUMBERS],
        }
    }

    pub fn full() -> NumberSet {
        NumberSet {
            accum: (1..(Number::NUM_NUMBERS + 1)).collect::<Vec<_>>().try_into().unwrap(),
        }
    }

    pub fn from_run(start: usize, length: usize) -> NumberSet {
        NumberSet {
            accum: iter::repeat(0).take(start)
                        .chain(1..(length + 1))
                        .chain(iter::repeat(length)).take(Number::NUM_NUMBERS)
                        .collect::<Vec<_>>().try_into().unwrap(),
        }
    }

    pub fn from_iter(numbers: impl Iterator<Item = Number>) -> NumberSet {
        let mut set = NumberSet::empty();
        numbers.for_each(|n| set += n);
        set
    }

    pub fn from_slice(numbers: &[Number]) -> NumberSet {
        NumberSet::from_iter(numbers.iter().copied())
    }

    pub fn single(number: Number) -> NumberSet {
        let mut set = NumberSet::empty();
        set += number;
        set
    }

    pub fn subset(&self, start: usize, length: usize) -> NumberSet {
        let mut subset = NumberSet::empty();
        let offset = self[start];
        for i in start..(start + length) {
            subset.accum[i] = self[i + 1] - offset;
        }
        for i in (start + length)..Number::NUM_NUMBERS {
            subset.accum[i] = subset[start + length];
        }
        subset
    }

    pub fn clear(&mut self) {
        self.accum = [0; Number::NUM_NUMBERS];
    }

    pub fn contains(&self, number: impl Borrow<Number>) -> bool {
        self.contains_by_index(number.borrow().index())
    }

    fn contains_by_index(&self, index: usize) -> bool {
        self[index] != self[index + 1]
    }

    pub fn lowest(&self) -> Option<Number> {
        self.iter().next()
    }

    pub fn highest(&self) -> Option<Number> {
        self.iter().rev().next()
    }

    pub fn len(&self) -> usize {
        self[Number::NUM_NUMBERS]
    }

    pub fn jokers_needed_for(&self, start: usize, length: usize) -> usize {
        length + self[start] - self[start + length]
    }

    /// returns an iterator of tuples (start, leading_empty); leading_empty is the number of empty
    /// slots before start
    fn iter_possible_starts(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        (0..Number::NUM_NUMBERS)
            .scan((false, 0), move |(before_empty, leading_empty), start| {
                // if the previous start value was empty, there is one more leading empty value
                *leading_empty = if *before_empty { *leading_empty + 1 } else { 0 };
                *before_empty = !self.contains_by_index(start);

                Some(if *before_empty { None } else { Some((start, *leading_empty)) })
            })
            .filter_map(identity)
    }

    /// returns an iterator of tuples (end, trailing_empty); trailing_empty is the number of empty
    /// slots after end
    fn iter_possible_ends(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        (0..Number::NUM_NUMBERS).rev()
            .scan((false, 0), move |(after_empty, trailing_empty), end| {
                // if the previous end (= start + length) value was empty, there is one more trailing empty value
                *trailing_empty = if *after_empty { *trailing_empty + 1 } else { 0 };
                *after_empty = !self.contains_by_index(end);

                Some(if *after_empty { None } else { Some((end + 1, *trailing_empty)) })  // end + 1 to make it an exclusive bound
            })
            .filter_map(identity)
    }

    /// returns Some((required_jokers, possible_jokers)) or None if the runs is invalid/impossible
    fn joker_counts(&self, start: usize, length: usize, bordering_empty: usize, num_jokers: usize) -> Option<(usize, usize)> {
        // calculate how many jokers we definitely need (for holes in the run), and make
        // sure that we have enough pieces (non-joker tiles) in the run
        let inner_jokers = self.jokers_needed_for(start, length);
        if length - inner_jokers < Set::MIN_PIECES {  // enough pieces in the run? we will add only jokers in the next lines
            return None;
        }

        // calculate how many outer jokers we can use (outer jokers are jokers at the start
        // or end of a run)
        let remaining_jokers = match num_jokers.checked_sub(inner_jokers) {
            Some(j) => j,  // the jokers not needed to fill some holes
            None => return None,  // too many jokers needed for this run
        };
        let min_outer_jokers = Set::MIN_LEN.saturating_sub(length);
        let max_outer_jokers = remaining_jokers.min(bordering_empty);
        let possible_outer_jokers = match max_outer_jokers.checked_sub(min_outer_jokers) {
            Some(j) => j,  // the number of possible outer joker counts
            None => return None,
        };

        Some((inner_jokers + min_outer_jokers, possible_outer_jokers))
    }

    /// returns a tuple of an array and a usize, the array represent how many times each number can
    /// used in a set, the usize represents how many times a joker can be used in a set (with the
    /// joker-assumption described in TileTable)
    pub fn num_possible_sets(&self, num_jokers: usize) -> ([usize; Number::NUM_NUMBERS], usize) {
        let mut number_counts = [0; Number::NUM_NUMBERS];
        let mut joker_count = 0;

        for (start, leading_empty) in self.iter_possible_starts() {
            for (end, trailing_empty) in self.iter_possible_ends() {
                let length = match end.checked_sub(start) {
                    Some(l) if l >= Set::MIN_PIECES => l,
                    _ => break,  // run is already too short, all subsequent run ends from this iterator will be too short aswell
                };

                if let Some((required_jokers, possible_jokers)) =
                        self.joker_counts(start, length, leading_empty + trailing_empty, num_jokers) {
                    joker_count += if required_jokers == 0 { 0 } else { 1 };  // do we _have_ to use jokers
                    joker_count += possible_jokers;

                    (0..Number::NUM_NUMBERS)
                        .filter(|&i| start <= i && i < start + length && self.contains_by_index(i))
                        .for_each(|i| number_counts[i] += possible_jokers + 1);
                }
            }
        }

        (number_counts, joker_count)
    }

    pub fn possible_sets(&self, number: Number, num_jokers: usize) -> impl Iterator<Item = (NumberSet, usize)> + '_ {
        let index = number.index();
        let number_contained = self.contains_by_index(index);

        self.iter_possible_starts()
            .take_while(move |&(start, _)| number_contained && start <= index)
            .flat_map(move |(start, leading_empty)|
                self.iter_possible_ends()
                    .take_while(move |&(end, _)| end > index)
                    .filter_map(move |(end, trailing_empty)| {
                        let length = end - start;
                        let subset = self.subset(start, length);
                        self.joker_counts(start, length, leading_empty + trailing_empty, num_jokers)
                            .map(|(required_jokers, possible_jokers)|
                                (required_jokers..(required_jokers + possible_jokers + 1)).map(move |j| (subset, j)))
                    })
                    .flat_map(identity))
    }

    pub fn iter(&self) -> impl DoubleEndedIterator<Item = Number> + '_ {
        Number::iter().filter(move |number| self.contains(*number))
    }
}

impl Index<usize> for NumberSet {
    type Output = usize;

    fn index(&self, index: usize) -> &usize {
        if index == 0 {
            &0
        }
        else {
            &self.accum[index - 1]
        }
    }
}

impl Index<Number> for NumberSet {
    type Output = bool;

    fn index(&self, number: Number) -> &bool {
        // we cant just return a reference to the result of self.contains as it's a temporary value
        if self.contains(number) {
            &true
        }
        else {
            &false
        }
    }
}

impl AddAssign<Number> for NumberSet {
    fn add_assign(&mut self, number: Number) {
        if !self.contains(number) {
            for count in self.accum[number.index()..].iter_mut() {
                *count += 1;
            }
        }
    }
}

impl AddAssign<NumberSet> for NumberSet {
    fn add_assign(&mut self, set: NumberSet) {
        let mut inc = 0;
        for number in Number::iter() {
            if !self.contains(number) && set.contains(number) {
                inc += 1;
            }
            self.accum[number.index()] += inc;
        }
    }
}

impl SubAssign<Number> for NumberSet {
    fn sub_assign(&mut self, number: Number) {
        for count in self.accum[number.index()..].iter_mut() {
            if *count != 0 {
                *count -= 1;
            }
        }
    }
}

impl SubAssign<NumberSet> for NumberSet {
    fn sub_assign(&mut self, set: NumberSet) {
        let mut dec = 0;
        for number in Number::iter() {
            if !self.contains(number) && set.contains(number) {
                dec += 1;
            }

            let count = &mut self.accum[number.index()];
            if *count != 0 {
                *count -= dec;
            }
        }
    }
}

impl Display for NumberSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.iter().map(|n| n.to_string()).collect::<Vec<_>>().join(NumberSet::SEPARATOR))
    }
}

impl FromStr for NumberSet {
    type Err = String;

    fn from_str(s: &str) -> Result<NumberSet, String> {
        Ok(NumberSet::from_slice(&s.split(NumberSet::SEPARATOR).map(|n| n.parse()).collect::<Result<Vec<_>, _>>()?))
    }
}

impl IntoIterator for NumberSet {
    type Item = Number;
    type IntoIter = impl DoubleEndedIterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        Number::iter().filter(move |number| self.contains(*number))
    }
}

impl Distribution<NumberSet> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> NumberSet {
        NumberSet::from_iter(numbers::ALL.iter().filter(|_| rng.gen()).copied())
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::tile::Tile;
    use rand::random;

    fn create(from: u8, to: u8, holes_at: &[u8]) -> NumberSet {
        let start = (from - 1) as usize;
        let length = (to - from + 1) as usize;

        let mut set = NumberSet::from_run(start, length);
        holes_at.iter().for_each(|&hole_at| set -= Number::from_value(hole_at).unwrap());
        set
    }

    #[test]
    fn test_from_run() {
        for start in 0..Number::NUM_NUMBERS {
            for length in 0..(Number::NUM_NUMBERS - start) {
                let set = NumberSet::from_run(start, length);
                assert_eq!(set.len(), length);
                (start..(start + length)).for_each(|index| assert!(set.contains_by_index(index)));
            }
        }
    }

    #[test]
    fn test_num_possible_sets() {
        let set = create(3, 8, &[7]);

        let (number_counts, joker_count) = set.num_possible_sets(0);
        assert_eq!(&number_counts[2..6], &[2, 3, 3, 2]);
        assert_eq!(joker_count, 0);

        let (number_counts, joker_count) = set.num_possible_sets(1);
        assert_eq!(&number_counts[2..8], &[6, 9, 10, 9, 0, 4]);
        assert_eq!(joker_count, 9);

        let (number_counts, joker_count) = set.num_possible_sets(2);
        assert_eq!(&number_counts[2..8], &[10, 14, 15, 14, 0, 8]);
        assert_eq!(joker_count, 16);
    }

    fn _test_possible_sets(set: NumberSet) {
        println!("{}", set);
        for num_jokers in 0..(Tile::NUM_COPIES + 1) {
            let (number_counts, _joker_count) = set.num_possible_sets(num_jokers);
            for (number, count) in Number::iter().zip(&number_counts) {
                let sets: Vec<_> = set.possible_sets(number, num_jokers).collect();
                println!("N{}, #J{}, {}", number, num_jokers, sets.iter().map(|s| format!("[{}]+{}J", s.0, s.1)).collect::<Vec<_>>().join(" "));
                assert_eq!(*count, sets.len(), "N{}, #J{}", number, num_jokers);
            }
        }
    }

    fn _test_subset(set: NumberSet) {
        for start in 0..Number::NUM_NUMBERS {
            for length in 0..(Number::NUM_NUMBERS - start) {
                let range = start..(start + length);
                let subset = set.subset(start, length);
                for (i, number) in Number::iter().enumerate() {
                    assert!(subset.contains(number) == (set.contains(number) && range.contains(&i)));
                }
            }
        }
    }

    #[test]
    fn test_possible_sets() {
        for _ in 0..32 {
            _test_possible_sets(random());
        }
    }

    #[test]
    fn test_subset() {
        for _ in 0..32 {
            _test_subset(random());
        }
    }
}
