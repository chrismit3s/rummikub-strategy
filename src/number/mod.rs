use std::fmt::{self, Display};
use std::iter::{DoubleEndedIterator};
use std::ops::Range;
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};


mod numberset;
pub use numberset::NumberSet;


#[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash, Debug)]
pub struct Number(usize);

impl Number {
    pub const MIN_NUMBER: u8 = 1;
    pub const MAX_NUMBER: u8 = 14;

    pub const NUM_NUMBERS: usize = (Number::MAX_NUMBER - Number::MIN_NUMBER) as usize;
    pub const RANGE: Range<u8> = Number::MIN_NUMBER..Number::MAX_NUMBER;

    pub fn from_value(value: u8) -> Result<Number, String> {
        if Number::RANGE.contains(&value) {
            Ok(Number((value - Number::MIN_NUMBER) as usize))
        }
        else {
            Err(format!("number {} out of range", value))
        }
    }

    pub fn from_index(index: usize) -> Result<Number, String> {
        if index < Number::NUM_NUMBERS {
            Ok(Number(index))
        }
        else {
            Err(format!("there is no number for index {}", index))
        }
    }

    pub fn value(&self) -> u8 {
        Number::MIN_NUMBER + self.0 as u8
    }

    pub fn index(&self) -> usize {
        self.0
    }

    pub fn iter() -> impl DoubleEndedIterator<Item = Number> {
        (0..Number::NUM_NUMBERS).map(Number)
    }
}

impl Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.value().fmt(f)
    }
}

impl FromStr for Number {
    type Err = String;

    fn from_str(s: &str) -> Result<Number, String> {
        s.parse::<u8>().map(Number::from_value).map_err(|_| format!("{} is an invalid piece value", s))?
    }
}

impl Distribution<Number> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Number {
        Number(rng.gen_range(0..Number::NUM_NUMBERS))
    }
}


pub mod values {
    use super::*;

    pub const N01: Number = Number(0);
    pub const N02: Number = Number(1);
    pub const N03: Number = Number(2);
    pub const N04: Number = Number(3);
    pub const N05: Number = Number(4);
    pub const N06: Number = Number(5);
    pub const N07: Number = Number(6);
    pub const N08: Number = Number(7);
    pub const N09: Number = Number(8);
    pub const N10: Number = Number(9);
    pub const N11: Number = Number(10);
    pub const N12: Number = Number(11);
    pub const N13: Number = Number(12);
    pub const ALL: &'static [Number] = &[
                    values::N01, values::N02, values::N03, values::N04, values::N05,
                    values::N06, values::N07, values::N08, values::N09, values::N10,
                    values::N11, values::N12, values::N13];
}


#[cfg(test)]
mod tests {
    //use super::*;
}
