use crate::tile::Tile;
use std::iter::Sum;
use std::fmt::Debug;


pub trait TableValue: Copy + PartialEq + Sum {
    const ZERO: Self;
    const ONE: Self;

    const DISPLAY_WIDTH: usize;

    type Error: Debug;

    fn add_checked(&self, other: Self) -> Result<Self, Self::Error>;
    fn sub_checked(&self, other: Self) -> Result<Self, Self::Error>;
    fn display(&self) -> String;
}

pub trait RangedTableValue: TableValue + Eq {
    const NUM_VALUES: usize;

    fn range_check(value: Self) -> Result<Self, Self::Error>;

    fn from_index(index: usize) -> Result<Self, Self::Error>;
    fn index(&self) -> usize;

    fn add_checked(&self, other: Self) -> Result<Self, Self::Error> {
        TableValue::add_checked(self, other).and_then(Self::range_check)
    }

    fn sub_checked(&self, other: Self) -> Result<Self, Self::Error> {
        TableValue::sub_checked(self, other).and_then(Self::range_check)
    }
}


impl TableValue for f32 {
    const ZERO: Self = 0.0;
    const ONE: Self = 1.0;

    const DISPLAY_WIDTH: usize = 5;

    type Error = !;

    fn add_checked(&self, other: Self) -> Result<Self, Self::Error> {
        Ok(self + other)
    }

    fn sub_checked(&self, other: Self) -> Result<Self, Self::Error> {
        Ok(self - other)
    }

    fn display(&self) -> String {
        if *self == Self::ZERO {
            format!("{:>w$}", "", w = Self::DISPLAY_WIDTH)
        }
        else {
            format!("{:>w$.2}", self, w = Self::DISPLAY_WIDTH)
        }
    }
}


impl TableValue for usize {
    const ZERO: Self = 0;
    const ONE: Self = 1;

    const DISPLAY_WIDTH: usize = 1;

    type Error = String;

    fn add_checked(&self, other: Self) -> Result<Self, Self::Error> {
        self.checked_add(other).ok_or_else(|| format!("Cannot add {} to {}", other, self))
    }

    fn sub_checked(&self, other: Self) -> Result<Self, Self::Error> {
        self.checked_sub(other).ok_or_else(|| format!("Cannot sub {} from {}", other, self))
    }

    fn display(&self) -> String {
        if *self == Self::ZERO {
            format!("{:>w$}", "", w = Self::DISPLAY_WIDTH)
        }
        else {
            format!("{:>w$}", self, w = Self::DISPLAY_WIDTH)
        }
    }
}

impl RangedTableValue for usize {
    const NUM_VALUES: usize = Tile::NUM_COPIES + 1;

    fn range_check(value: Self) -> Result<Self, Self::Error> {
        if value <= Tile::NUM_COPIES {
            Ok(value)
        }
        else {
            Err(format!("{} is out of range 0..={}", value, Tile::NUM_COPIES))
        }
    }

    fn from_index(index: usize) -> Result<Self, Self::Error> {
        Self::range_check(index)
    }

    fn index(&self) -> usize {
        *self
    }
}


impl TableValue for isize {
    const ZERO: Self = 0;
    const ONE: Self = 1;

    const DISPLAY_WIDTH: usize = 2;

    type Error = String;

    fn add_checked(&self, other: Self) -> Result<Self, Self::Error> {
        self.checked_add(other).ok_or_else(|| format!("Cannot add {} to {}", other, self))
    }

    fn sub_checked(&self, other: Self) -> Result<Self, Self::Error> {
        self.checked_sub(other).ok_or_else(|| format!("Cannot sub {} from {}", other, self))
    }

    fn display(&self) -> String {
        if *self == Self::ZERO {
            format!("{:>w$}", "", w = Self::DISPLAY_WIDTH)
        }
        else {
            format!("{:+>w$}", self, w = Self::DISPLAY_WIDTH - 1)  // -1 for the plus/minus sign
        }
    }
}

impl RangedTableValue for isize {
    const NUM_VALUES: usize = Tile::NUM_COPIES * 2 + 1;

    fn range_check(value: Self) -> Result<Self, Self::Error> {
        if value.unsigned_abs() <= Tile::NUM_COPIES {
            Ok(value)
        }
        else {
            Err(format!("{} is out of range {}..={}", value, -(Tile::NUM_COPIES as isize), Tile::NUM_COPIES))
        }
    }

    fn from_index(index: usize) -> Result<Self, Self::Error> {
        Self::range_check(index as isize - Tile::NUM_COPIES as isize)
    }

    fn index(&self) -> usize {
        (self + Tile::NUM_COPIES as isize) as usize
    }
}
