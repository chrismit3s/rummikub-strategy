use crate::color::Color;
use crate::number::Number;
use crate::tile::{Tile, TileIter};
use crate::piece::{Piece, PieceIter};
use std::fmt::{self, Display, LowerHex, UpperHex};
use std::iter::{self, Iterator, IntoIterator, Copied, Zip};
use std::ops::Index;
use std::str::FromStr;
use rand::distributions::{Distribution, Standard};


mod tiletable;
pub use tiletable::TileTable;

mod settable;
pub use settable::SetTable;

mod index;
pub use index::TableIndex;

mod value;
pub use value::{TableValue, RangedTableValue};


pub type TableIter<T> = <T as IntoIterator>::IntoIter;


pub trait Table: IntoIterator<Item = Self::Value> + Index<usize, Output = Self::Value> + Display + Sized
        where for<'a> &'a Self: IntoIterator<Item = &'a Self::Value>,
              for<'a> &'a mut Self: IntoIterator<Item = &'a mut Self::Value> {
    type Value: TableValue;

    fn empty() -> Self;
    fn clear(&mut self);

    fn add_tile<T: TableIndex>(&mut self, t: T, v: Self::Value)
            -> Result<(), <Self::Value as TableValue>::Error>;
    fn remove_tile<T: TableIndex>(&mut self, t: T, v: Self::Value)
            -> Result<(), <Self::Value as TableValue>::Error>;

    fn from_iter<T: TableIndex, I: IntoIterator<Item = T>>(i: I)
            -> Result<Self, <Self::Value as TableValue>::Error> {
        let mut table = Self::empty();
        table.add_iter(i, Self::Value::ONE)?;
        Ok(table)
    }

    fn from_tiles<T: TableIndex, I: IntoIterator<Item = (T, Self::Value)>>(i: I)
            -> Result<Self, <Self::Value as TableValue>::Error> {
        let mut table = Self::empty();
        table.add_tiles(i)?;
        Ok(table)
    }

    fn add_iter<T: TableIndex, I: IntoIterator<Item = T>>(&mut self, i: I, v: Self::Value)
            -> Result<(), <Self::Value as TableValue>::Error> {
        i.into_iter().try_for_each(|t| self.add_tile(t, v))
    }

    fn remove_iter<T: TableIndex, I: IntoIterator<Item = T>>(&mut self, i: I, v: Self::Value)
            -> Result<(), <Self::Value as TableValue>::Error> {
        i.into_iter().try_for_each(|t| self.remove_tile(t, v))
    }

    fn add_tiles<T: TableIndex, I: IntoIterator<Item = (T, Self::Value)>>(&mut self, i: I)
            -> Result<(), <Self::Value as TableValue>::Error> {
        i.into_iter().try_for_each(|(t, v)| self.add_tile(t, v))
    }

    fn remove_tiles<T: TableIndex, I: IntoIterator<Item = (T, Self::Value)>>(&mut self, i: I)
            -> Result<(), <Self::Value as TableValue>::Error> {
        i.into_iter().try_for_each(|(t, v)| self.remove_tile(t, v))
    }

    fn contains<T: TableIndex>(&self, t: T) -> bool {
        self[t.index()] != Self::Value::ZERO
    }

    fn num_jokers(&self) -> Self::Value {
        self[Tile::Joker.index()]
    }

    fn total(&self) -> Self::Value {
        self.iter().copied().sum()
    }

    fn iter(&self) -> TableIter<&'_ Self> {
        self.into_iter()
    }

    fn iter_mut(&mut self) -> TableIter<&'_ mut Self> {
        self.into_iter()
    }

    fn iter_piece_values(&self) -> Zip<PieceIter, Copied<TableIter<&'_ Self>>> {
        Piece::iter().zip(self.iter().copied())
    }

    fn iter_tile_values(&self) -> Zip<TileIter, Copied<TableIter<&'_ Self>>> {
        Tile::iter().zip(self.iter().copied())
    }

    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const ROW_HEAD_WIDTH: usize = 6;  // longest header is "Yellow"
        const CELL_WIDTH: usize = 2;  // longest number has 2 digits

        let cell_width = Self::Value::DISPLAY_WIDTH.max(CELL_WIDTH);

        // if there is a title if the upper left corner of this table, its width should be
        // specified as precision in the formatter so that the table lines up neatly
        write!(f, "{:>r$}", "", r = ROW_HEAD_WIDTH.saturating_sub(f.precision().unwrap_or(0)))?;
        Number::iter().try_for_each(|n| write!(f, " {:>c$}", n, c = cell_width))?;
        writeln!(f)?;

        for color in Color::iter() {
            write!(f, "{:>r$}", color, r = ROW_HEAD_WIDTH)?;
            for number in Number::iter() {
                write!(f, " {:>c$}",
                       Self::Value::display(&self[Piece::index_for(number, color)]),
                       c = cell_width)?;
            }
            writeln!(f)?;
        }

        write!(f, "{:>r$}", Tile::Joker, r = ROW_HEAD_WIDTH)?;
        write!(f, " {:>c$}", self[Tile::Joker.index()].display(), c = cell_width)?;
        writeln!(f)?;

        Ok(())
    }
}

pub trait RangedTable<V: RangedTableValue>: Table<Value = V> + FromStr + LowerHex + UpperHex + Eq
        where for<'a> &'a Self: IntoIterator<Item = &'a Self::Value>,
              for<'a> &'a mut Self: IntoIterator<Item = &'a mut Self::Value>,
              Standard: Distribution<Self> {
    fn from_int(x: u128)
            -> Result<Self, <Self::Value as TableValue>::Error> {
        let y = Self::Value::NUM_VALUES as u128;
        Self::from_tiles(Tile::iter().rev()
                                .zip(iter::successors(Some(x), |x| Some(x / y))
                                        .map(|x| Self::Value::from_index((x % y) as usize).unwrap())))
    }

    fn to_int(&self) -> u128 {
        let y = Self::Value::NUM_VALUES as u128;
        self.iter().fold(0u128, |x, v| x * y + (v.index() as u128))
    }
}

impl<V: RangedTableValue, T: Table<Value = V> + FromStr + LowerHex + UpperHex + Eq> RangedTable<V> for T
        where for<'a> &'a Self: IntoIterator<Item = &'a Self::Value>,
              for<'a> &'a mut Self: IntoIterator<Item = &'a mut Self::Value>,
              Standard: Distribution<Self> {}
