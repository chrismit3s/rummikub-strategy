use crate::table::{Table, RangedTable, TableIndex, TableValue, RangedTableValue};
use crate::tile::Tile;
use crate::piece::Piece;
use std::array::IntoIter;
use std::fmt::{self, Display, LowerHex, UpperHex};
use std::iter::{self, Iterator, IntoIterator};
use std::ops::{Index, IndexMut};
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};


// The general assumption for jokers in this entire project is that jokers will only be used
// for tiles you dont already have. This actually makes a lot of sense and simplifies almost
// everything.
#[derive(Clone, Copy, PartialEq, Hash, Debug)]
pub struct TileTable<V: TableValue>([V; Tile::NUM_TILES]);

impl TileTable<usize> {
    pub fn len(&self) -> usize {
        self.total()
    }

    pub fn iter_pieces(&self) -> impl Iterator<Item = Piece> + '_ {
        self.iter_piece_values().flat_map(|(p, v)| iter::repeat(p).take(v))
    }

    pub fn iter_tiles(&self) -> impl Iterator<Item = Tile> + '_ {
        self.iter_tile_values().flat_map(|(t, v)| iter::repeat(t).take(v))
    }
}

impl<V: TableValue> Table for TileTable<V> {
    type Value = V;

    fn empty() -> Self {
        TileTable([V::ZERO; Tile::NUM_TILES])
    }

    #[inline]
    fn clear(&mut self) {
        self.0.fill(V::ZERO)
    }

    #[inline]
    fn add_tile<T: TableIndex>(&mut self, t: T, v: V) -> Result<(), V::Error> {
        self[t] = self[t].add_checked(v)?;
        Ok(())
    }

    #[inline]
    fn remove_tile<T: TableIndex>(&mut self, t: T, v: V) -> Result<(), V::Error> {
        self[t] = self[t].sub_checked(v)?;
        Ok(())
    }
}

impl<V: TableValue> Display for TileTable<V> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Table::fmt(self, f)
    }
}

impl<V: TableValue> IntoIterator for TileTable<V> {
    type Item = V;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter::new(self.0)
    }
}

impl<'a, V: TableValue> IntoIterator for &'a TileTable<V> {
    type Item = &'a V;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<'a, V: TableValue> IntoIterator for &'a mut TileTable<V> {
    type Item = &'a mut V;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}

impl<T: TableIndex, V: TableValue> Index<T> for TileTable<V> {
    type Output = V;

    fn index(&self, t: T) -> &Self::Output {
        &self.0[t.index()]
    }
}

impl<T: TableIndex, V: TableValue> IndexMut<T> for TileTable<V> {
    fn index_mut(&mut self, t: T) -> &mut Self::Output {
        &mut self.0[t.index()]
    }
}

impl<V: RangedTableValue> LowerHex for TileTable<V> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:x}", self.to_int())
    }
}

impl<V: RangedTableValue> UpperHex for TileTable<V> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:X}", self.to_int())
    }
}

impl<V: RangedTableValue> Eq for TileTable<V> {}

impl<V: RangedTableValue> FromStr for TileTable<V> {
    type Err = String;

    fn from_str(hex: &str) -> Result<Self, Self::Err> {
        let x = u128::from_str_radix(hex, 16).map_err(|e| format!("{:?}", e))?;
        TileTable::from_int(x).map_err(|e| format!("{:?}", e))
    }
}

impl<V: RangedTableValue> Distribution<TileTable<V>> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> TileTable<V> {
        TileTable::from_tiles(
                Tile::iter().map(|t|
                        (t, V::from_index(
                                rng.gen_range(0..V::NUM_VALUES)).unwrap()))).unwrap()
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    use rand::{random, thread_rng};

    #[test]
    fn test_full_table_space_in_eight_bytes() {
        assert!((Tile::NUM_TILES as u128).checked_pow(usize::NUM_VALUES as u32).unwrap() <= u128::MAX);
    }

    #[test]
    fn test_to_from_fixpoint() {
        for _ in 0..32 {
            let table: TileTable<usize> = random();
            assert_eq!(Ok(table), TileTable::from_int(table.to_int()));
            assert_eq!(Ok(table), format!("{:x}", table).parse());
        }
    }

    #[test]
    fn test_from_to_int_fixpoint() {
        let max = (Tile::NUM_TILES as u128).checked_pow(usize::NUM_VALUES as u32).unwrap();
        let mut rng = thread_rng();
        for _ in 0..32 {
            let x: u128 = rng.gen_range(0..max);
            assert_eq!(x, TileTable::<usize>::from_int(x).unwrap().to_int());
        }
    }
}
