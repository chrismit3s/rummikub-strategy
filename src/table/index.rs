use std::fmt::Display;

pub trait TableIndex: Copy + Display {
    fn index(&self) -> usize;
}

impl TableIndex for usize {
    fn index(&self) -> usize {
        *self
    }
}
