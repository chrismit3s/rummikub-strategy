use crate::color::{Color, ColorSet};
use crate::number::{Number, NumberSet};
use crate::piece::Piece;
use crate::set::Set;
use crate::table::{TileTable, Table, TableIndex};
use crate::tile::Tile;
use std::collections::HashMap;
use std::fmt::{self, Display, LowerHex, UpperHex};
use std::iter::{self, Iterator};
use std::ops::Index;
use std::str::FromStr;
use rand::Rng;
use rand::distributions::{Distribution, Standard};


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct SetTable {
    len: usize,
    tiles: TileTable<usize>,
    colorsets: [ColorSet; Number::NUM_NUMBERS],
    numbersets: [NumberSet; Color::NUM_COLORS],
}

impl SetTable {
    pub fn from_table(tiles: TileTable<usize>) -> SetTable {
        let mut settable = SetTable {
            tiles,
            ..SetTable::empty()
        };

        let len = &mut settable.len;
        let tiles = &settable.tiles;
        let colorsets = &mut settable.colorsets;
        let numbersets = &mut settable.numbersets;

        *len += tiles[Tile::Joker];
        for (Piece { number, color }, count) in tiles.iter_piece_values().filter(|&(_p, c)| c > 0) {
            *len += count;
            colorsets[number.index()] += color;
            numbersets[color.index()] += number;
        }

        settable
    }

    pub fn from_slice(tiles: &[Tile]) -> Result<SetTable, String> {
        SetTable::from_iter(tiles.iter().map(|t| *t))
    }

    pub fn from_sets(sets: &HashMap<Set, usize>) -> Result<SetTable, String> {
        SetTable::from_iter(sets.iter()
                                .flat_map(|(s, &c)| iter::repeat(s.clone()).take(c))
                                .flat_map(|s| s.into_iter()))
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn tiles(&self) -> &TileTable<usize> {
        &self.tiles
    }

    pub fn colorset(&self, number: Number) -> &ColorSet {
        &self.colorsets[number.index()]
    }

    pub fn colorset_mut(&mut self, number: Number) -> &mut ColorSet {
        &mut self.colorsets[number.index()]
    }

    pub fn numberset(&self, color: Color) -> &NumberSet {
        &self.numbersets[color.index()]
    }

    pub fn numberset_mut(&mut self, color: Color) -> &mut NumberSet {
        &mut self.numbersets[color.index()]
    }

    pub fn tiles_missing_for(&self, set: &Set) -> usize {
        set.iter().filter(|tile| !self.contains(tile)).count()
    }

    pub fn num_possible_sets(&self) -> TileTable<usize> {
        let mut table = TileTable::empty();

        for (color, numberset) in Color::iter().zip(&self.numbersets) {
            let (number_counts, j) = numberset.num_possible_sets(self.tiles.num_jokers());
            table[Tile::Joker] += j;
            for (number, n) in Number::iter().zip(&number_counts) {
                table[Piece::new(number, color)] += n;
            }
        }

        for (number, colorset) in Number::iter().zip(&self.colorsets) {
            let (color_counts, j) = colorset.num_possible_sets(self.tiles.num_jokers());
            table[Tile::Joker] += j;
            for (color, c) in Color::iter().zip(&color_counts) {
                table[Piece::new(number, color)] += c;
            }
        }

        table
    }

    pub fn possible_sets_for(&self, piece: Piece) -> impl Iterator<Item = Set> + '_ {
        let runs = self.numberset(piece.color).possible_sets(piece.number, self.tiles.num_jokers())
                                .map(move |(set, j)| Set::run(set, piece.color, j).unwrap());
        let groups = self.colorset(piece.number).possible_sets(piece.color, self.tiles.num_jokers())
                                .map(move |(set, j)| Set::group(piece.number, set, j).unwrap());
        runs.chain(groups)
    }

    pub fn is_possible(&mut self) -> Option<HashMap<Set, usize>> {
        // there are no pieces
        if self.len() == 0 {
            return Some(HashMap::new());
        }
        else if self.len() == self.num_jokers() {
            return None;
        }

        let set_counts = self.num_possible_sets();
        let (best_piece, _least_num_sets) =
                set_counts.iter_piece_values()
                    .filter(|&(p, _i)| self.tiles[p] != 0)
                    .min_by_key(|&(_p, i)| i)
                    .unwrap();

        for set in self.possible_sets_for(best_piece).collect::<Vec<_>>().into_iter() {
            self.remove_iter(set.iter(), 1).unwrap();
            let ret = self.is_possible();
            self.add_iter(set.iter(), 1).unwrap();

            if let Some(mut sets) = ret {
                *sets.entry(set.clone()).or_insert(0) += 1;
                return Some(sets);
            }
        }

        None
    }

    #[inline]
    pub fn is_possible_with_optional(&mut self, skippable: &mut TileTable<usize>)
            -> Option<(HashMap<Set, usize>, TileTable<usize>, usize)> {
        self._is_possible_with_optional(skippable, usize::MAX)
    }

    fn _is_possible_with_optional(&mut self, skippable: &mut TileTable<usize>, mut least_value_skipped: usize)
            -> Option<(HashMap<Set, usize>, TileTable<usize>, usize)> {
        // there are no pieces
        if self.len() == 0 {
            return Some((HashMap::new(), TileTable::empty(), 0));
        }
        else if self.len() == self.num_jokers() {
            return None;
        }

        let (piece, _) = self.iter_piece_values().filter(|&(_, i)| i != 0).next().unwrap();
        let piece_value = piece.value() as usize;

        let mut sets_and_skips = None;
        for set in self.possible_sets_for(piece).collect::<Vec<_>>().into_iter() {
            self.remove_iter(set.iter(), 1).unwrap();

            if let Some((mut sets, tiles_skipped, value_skipped))
                    = self._is_possible_with_optional(skippable, least_value_skipped) {
                if least_value_skipped > value_skipped {
                    *sets.entry(set.clone()).or_insert(0) += 1;

                    least_value_skipped = value_skipped;
                    sets_and_skips = Some((sets, tiles_skipped, least_value_skipped));
                }
            }

            self.add_iter(set.iter(), 1).unwrap();
        }

        if skippable.contains(piece) && piece_value < least_value_skipped {
            self.remove_tile(piece, 1).unwrap();
            skippable.remove_tile(piece, 1).unwrap();

            if let Some((sets, mut tiles_skipped, mut value_skipped))
                    = self._is_possible_with_optional(skippable, least_value_skipped - piece_value) {
                if least_value_skipped > value_skipped {
                    value_skipped += piece.value() as usize;
                    tiles_skipped.add_tile(piece, 1).unwrap();

                    least_value_skipped = value_skipped;
                    sets_and_skips = Some((sets, tiles_skipped, least_value_skipped));
                }
            }

            skippable.add_tile(piece, 1).unwrap();
            self.add_tile(piece, 1).unwrap();
        }

        sets_and_skips
    }

    pub fn iter_pieces(&self) -> impl Iterator<Item = Piece> + '_ {
        self.tiles.iter_pieces()
    }

    pub fn iter_tiles(&self) -> impl Iterator<Item = Tile> + '_ {
        self.tiles.iter_tiles()
    }
}

impl Table for SetTable {
    type Value = usize;

    fn empty() -> SetTable {
        SetTable {
            len: 0,
            tiles: TileTable::empty(),
            colorsets: [ColorSet::empty(); Number::NUM_NUMBERS],
            numbersets: [NumberSet::empty(); Color::NUM_COLORS],
        }
    }

    fn clear(&mut self) {
        self.len = 0;
        self.tiles.clear();
        self.colorsets.iter_mut().for_each(|c| c.clear());
        self.numbersets.iter_mut().for_each(|n| n.clear());
    }

    fn total(&self) -> usize {
        self.len
    }

    fn add_tile<T: TableIndex>(&mut self, tile: T, n: usize) -> Result<(), String> {
        self.tiles.add_tile(tile, n)?;
        self.len += n;

        // increase by a nonzero amount
        if self.tiles[tile] != 0 {
            if let Ok(Tile::Piece(Piece { number, color })) = Tile::from_index(tile.index()) {
                *self.colorset_mut(number) += color;
                *self.numberset_mut(color) += number;
            }
        }

        Ok(())
    }

    fn remove_tile<T: TableIndex>(&mut self, tile: T, n: usize) -> Result<(), String> {
        self.tiles.remove_tile(tile, n)?;
        self.len -= n;

        // decreased to zero
        if self.tiles[tile] == 0 {
            if let Ok(Tile::Piece(Piece { number, color })) = Tile::from_index(tile.index()) {
                *self.colorset_mut(number) -= color;
                *self.numberset_mut(color) -= number;
            }
        }

        Ok(())
    }
}

impl Display for SetTable {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(&self.tiles, f)
    }
}

impl IntoIterator for SetTable {
    type Item = usize;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIterator::into_iter(self.tiles)
    }
}

impl<'a> IntoIterator for &'a SetTable {
    type Item = &'a usize;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIterator::into_iter(&self.tiles)
    }
}

impl<'a> IntoIterator for &'a mut SetTable {
    type Item = &'a mut usize;
    type IntoIter = impl Iterator<Item = Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIterator::into_iter(&mut self.tiles)
    }
}

impl<I: TableIndex> Index<I> for SetTable {
    type Output = usize;

    fn index(&self, index: I) -> &usize {
        Index::index(&self.tiles, index)
    }
}

impl LowerHex for SetTable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        LowerHex::fmt(&self.tiles, f)
    }
}

impl UpperHex for SetTable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        UpperHex::fmt(&self.tiles, f)
    }
}

impl FromStr for SetTable {
    type Err = String;

    fn from_str(hex: &str) -> Result<Self, String> {
        hex.parse().map(SetTable::from_table)
    }
}

impl Distribution<SetTable> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> SetTable {
        let mut table = SetTable::empty();
        table.add_iter(rng.gen::<Set>().into_iter(), 1).unwrap();

        while rng.gen_ratio(1, 10) {
            let set: Set = rng.gen();
            if set.iter().all(|tile| table[tile] < Tile::NUM_COPIES)
                    && table.num_jokers() + set.num_jokers() <= Tile::NUM_COPIES {
                table.add_iter(set.into_iter(), 1).unwrap();
            }
        }
        table
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    extern crate test;

    use crate::tile::values as tiles;
    use test::Bencher;
    use rand::random;
    use rand::seq::IteratorRandom;
    use rand_pcg::Pcg64Mcg;

    #[test]
    fn test_full_hand_two_bytes() {
        assert!(Tile::MAX_TILES_ON_HAND <= 16);
    }

    #[test]
    fn test_is_possible() {
        let mut table = SetTable::from_slice(
                &[tiles::BLK04, tiles::BLK05, tiles::BLK06, tiles::BLK07,
                  tiles::YLW07, tiles::RED07, tiles::BLU07]).unwrap();
        assert_eq!(table.is_possible().map(|h| h.len()), Some(2));

        let mut table = SetTable::from_slice(
                &[tiles::BLU03, tiles::RED03, tiles::YLW03,
                  tiles::YLW04, tiles::YLW05, tiles::YLW06,
                  tiles::BLU06, tiles::BLU07, tiles::BLU08,
                  tiles::BLU08, tiles::RED08, tiles::BLK08]).unwrap();
        assert_eq!(table.is_possible().map(|h| h.len()), Some(4));

        let mut table = SetTable::from_slice(
                &[tiles::BLU03, tiles::RED03, tiles::YLW03,
                  tiles::YLW04, tiles::YLW05, tiles::YLW06,
                  tiles::BLU06, tiles::BLU07, tiles::BLU08,
                  tiles::BLU08, tiles::RED08, tiles::BLK08,
                  tiles::BLU12, tiles::RED12, tiles::BLK12,
                  tiles::YLW12, tiles::RED12, tiles::BLK12]).unwrap();
        assert_eq!(table.is_possible().map(|h| h.len()), Some(6));

        for _ in 0..64 {
            let mut table: SetTable = random();
            assert!(table.is_possible().is_some());
        }
    }

    #[test]
    fn test_is_possible_with_optional() {
        let mut table = SetTable::from_slice(
                &[tiles::BLK04, tiles::BLK05, tiles::BLK06, tiles::BLK07,
                  tiles::YLW07, tiles::RED07, tiles::BLU07]).unwrap();
        assert_eq!(table.is_possible_with_optional(&mut TileTable::empty()).map(|(h, _, x)| (h.len(), x)), Some((2, 0)));

        let mut table = SetTable::from_slice(
                &[tiles::BLU03, tiles::RED03, tiles::YLW03,
                  tiles::YLW04, tiles::YLW05, tiles::YLW06,
                  tiles::BLU06, tiles::BLU07, tiles::BLU08,
                  tiles::BLU08, tiles::RED08, tiles::BLK08]).unwrap();
        assert_eq!(table.is_possible_with_optional(&mut TileTable::empty()).map(|(h, _, x)| (h.len(), x)), Some((4, 0)));

        let mut table = SetTable::from_slice(
                &[tiles::BLU03, tiles::RED03, tiles::YLW03,
                  tiles::YLW04, tiles::YLW05, tiles::YLW06,
                  tiles::BLU06, tiles::BLU07, tiles::BLU08,
                  tiles::BLU08, tiles::RED08, tiles::BLK08,
                  tiles::BLU12, tiles::RED12, tiles::BLK12,
                  tiles::YLW12, tiles::RED12, tiles::BLK12]).unwrap();
        assert_eq!(table.is_possible_with_optional(&mut TileTable::empty()).map(|(h, _, x)| (h.len(), x)), Some((6, 0)));

        let mut table = SetTable::from_slice(
                &[tiles::BLK04, tiles::BLK05, tiles::BLK06, tiles::BLK07,
                  tiles::YLW07, tiles::RED07, tiles::BLU07, tiles::BLK10]).unwrap();
        let mut skip = TileTable::from_iter(&[tiles::BLK10]).unwrap();
        assert_eq!(table.is_possible_with_optional(&mut skip).map(|(h, _, x)| (h.len(), x)), Some((2, 10)));

        let mut table = SetTable::from_slice(
                &[tiles::BLU03, tiles::RED03, tiles::YLW03,
                  tiles::YLW04, tiles::YLW05, tiles::YLW06,
                  tiles::BLU06, tiles::BLU07, tiles::BLU08,
                  tiles::BLU08, tiles::RED08, tiles::BLK08,
                  tiles::BLK10]).unwrap();
        let mut skip = TileTable::from_iter(&[tiles::BLK10]).unwrap();
        assert_eq!(table.is_possible_with_optional(&mut skip).map(|(h, _, x)| (h.len(), x)), Some((4, 10)));

        let mut table = SetTable::from_slice(
                &[tiles::BLU03, tiles::RED03, tiles::YLW03,
                  tiles::YLW04, tiles::YLW05, tiles::YLW06,
                  tiles::BLU06, tiles::BLU07, tiles::BLU08,
                  tiles::BLU08, tiles::RED08, tiles::BLK08,
                  tiles::BLU12, tiles::RED12, tiles::BLK12,
                  tiles::YLW12, tiles::RED12, tiles::BLK12,
                  tiles::BLK10]).unwrap();
        let mut skip = TileTable::from_iter(&[tiles::BLK10]).unwrap();
        assert_eq!(table.is_possible_with_optional(&mut skip).map(|(h, _, x)| (h.len(), x)), Some((6, 10)));

        for _ in 0..64 {
            let mut table: SetTable = random();
            let ret = table.is_possible_with_optional(&mut TileTable::empty());
            assert!(ret.is_some());
            assert_eq!(ret.unwrap().2, 0);
        }
    }

    #[bench]
    fn bench_is_possible_guaranteed(b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(0xBADF00DBABE);
        b.iter(move || {
            let mut table: SetTable = rng.gen();
            table.is_possible()
        })
    }

    #[bench]
    fn bench_is_possible_modified(b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(0xBADF00DBABE);
        b.iter(move || {
            let mut table: SetTable = rng.gen();

            let (tile, value) = table.iter_tile_values().choose(&mut rng).unwrap();
            if value == Tile::NUM_COPIES {
                table.remove_tile(tile, 1).unwrap();
            }
            else {
                table.add_tile(tile, 1).unwrap();
            }

            table.is_possible()
        })
    }
}
